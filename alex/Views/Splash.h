//
//  Splash.h
//  alex
//
//  Created by Alex on 14.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@protocol SplashDelegate <NSObject>
-(void)removeSplash;
@end

@interface Splash : UIView

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) id delegate;
-(id) initWithDelegate:(id <SplashDelegate>) delegate;
-(void)animate;
-(void)hide;
@end
