//
//  genreTableViewCell.m
//  alex
//
//  Created by Alex on 02.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "GenreTableViewCell.h"

@implementation GenreTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    //[super setSelected:selected animated:animated];    
}

- (void)setContentForGenre:(Genre *)genre andChecked:(BOOL)checked andIndex:(NSInteger)index {
    self.genreLabel.text = genre.name;
    self.genreLabel.textColor = GREENCOLOR;
    self.genreSwitch.on = checked;
    [self.genreSwitch setTag:index];
    [self.genreSwitch addTarget:self action:@selector(setState:) forControlEvents:UIControlEventValueChanged];
}

- (void)setState:(id)sender
{
    UISwitch *genreSwitch = sender;
    BOOL state = [genreSwitch isOn];
    NSInteger index = genreSwitch.tag;
    [self.delegate genreWasSetTo:state withIndex:index];
}

@end
