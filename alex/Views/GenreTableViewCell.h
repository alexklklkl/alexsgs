//
//  genreTableViewCell.h
//  alex
//
//  Created by Alex on 02.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Genre.h"
#import "Constants.h"

@protocol GenreTableViewCellDelegate <NSObject>
-(void)genreWasSetTo:(BOOL)checked withIndex:(NSInteger)index;
@end

@interface GenreTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *genreSwitch;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (nonatomic, assign) id <GenreTableViewCellDelegate> delegate;

- (void)setContentForGenre:(Genre *)genre andChecked:(BOOL)checked andIndex:(NSInteger)index;
//- (void)setDelegate:(id <GenreTableViewCellDelegate>) delegate;

@end
