//
//  FilmTableViewCell.m
//  alex
//
//  Created by Alex on 17.11.16.
//  Copyright © 2016 kode. All rights reserved.
//

#import "Constants.h"
#import "FilmTableViewCell.h"
#import "Film.h"

@implementation FilmTableViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setContentForFilm:(Film *)film {
    [self.filmLabel setText:[film.name uppercaseString]];
    [self.genreLabel setText:film.genries];
    CGFloat ratingF;
    @try {
        ratingF = [film.rating floatValue];
    } @catch (NSException *exception) {
        ratingF = 0;
    }
    NSInteger rating = round(ratingF);
    if (rating >= 0) {
        [self.starsImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"star_%ld.png", (long)rating]]];
    } else {
        [self.starsImageView setImage:[UIImage imageNamed:@"transparent.png"]];
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *imagePath = [[Helper getBasePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.jpg", IMAGEPATH, film.idFilm]];
    NSString *imagePathTh = [[Helper getBasePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@_th.jpg", IMAGEPATH, film.idFilm]];
    if ([fileManager fileExistsAtPath:imagePathTh]) {
        [self.filmImageView setImage:[UIImage imageWithContentsOfFile:imagePathTh]];
        [self.filmImageView setAlpha:IMAGEALPHA];
    } else {
        if ([fileManager fileExistsAtPath:imagePath]) {
            UIImage *image = [Helper resizeImage:[UIImage imageWithContentsOfFile:imagePath] andRatio:CELL_RATIO];
            [UIImageJPEGRepresentation(image, 0.7) writeToFile:imagePathTh atomically:YES];
            [self.filmImageView setImage:image];
            [self.filmImageView setAlpha:IMAGEALPHA];
        } else {
            [self.filmImageView setImage:[Helper resizeImage:[UIImage imageNamed:@"transparent.png"] andRatio:CELL_RATIO]];
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    //[super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
