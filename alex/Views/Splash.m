//
//  Splash.m
//  alex
//
//  Created by Alex on 14.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "Splash.h"

@implementation Splash

-(id) initWithDelegate:(id<SplashDelegate>)delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
        CGRect frame = [[UIScreen mainScreen] bounds];
        [self setBackgroundColor:GRAYCOLOR];
        self.frame = frame;
        self.imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        self.imageView.frame = CGRectMake(frame.size.width * .2, frame.size.height * .2, frame.size.width * .6, frame.size.height * .6);
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.imageView];
        
    }
    return self;
}

-(void)animate {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.toValue = [NSNumber numberWithFloat: M_PI * 2.0];
    animation.duration = 5;
    animation.cumulative = YES;
    animation.repeatCount = 100;
    animation.beginTime = CACurrentMediaTime() + .5;
    [self.imageView.layer addAnimation:animation forKey:@"rotationAnimation"];
}

-(void)hide {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.fromValue = [NSNumber numberWithFloat:1.0];
    animation.toValue = [NSNumber numberWithFloat:0.0];
    animation.duration = 0.5;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    [CATransaction setCompletionBlock:^{
        [self.imageView.layer removeAllAnimations];
        [self.layer removeAllAnimations];
        [self.delegate removeSplash];
    }];
    [self.layer addAnimation:animation forKey:@"opacityAnimation"];}

@end
