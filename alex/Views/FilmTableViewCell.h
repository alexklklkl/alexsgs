//
//  FilmTableViewCell.h
//  alex
//
//  Created by Alex on 17.11.16.
//  Copyright © 2016 kode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "Film.h"

@interface FilmTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *filmImageView;
@property (weak, nonatomic) IBOutlet UILabel *filmLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starsImageView;

- (void)setContentForFilm:(Film *)film;
@end
