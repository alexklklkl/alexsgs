//
//  SelectCityTableViewController.m
//  alex
//
//  Created by Alex on 02.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "SelectCityTableViewController.h"

@interface SelectCityTableViewController ()

@end

@implementation SelectCityTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Ваш город", nil);
    self.view.backgroundColor = GRAYCOLOR;
    self.tableView.sectionIndexBackgroundColor = GRAYCOLOR;
    self.tableView.sectionIndexColor = GREENCOLOR;
    self.navigationController.navigationBar.barTintColor = GRAYCOLOR;
    self.navigationController.navigationBar.tintColor = GREENCOLOR;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationItem.rightBarButtonItem setTitle:@"Выбрать"];
    
    self.alphabetizedDictionary = [CGLAlphabetizer alphabetizedDictionaryFromObjects: [self.cities getCities] usingKeyPath:NAME];
    self.sectionIndexTitles = [CGLAlphabetizer indexTitlesFromAlphabetizedDictionary:self.alphabetizedDictionary];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (City *)objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *sectionIndexTitle = self.sectionIndexTitles[indexPath.section];
    return self.alphabetizedDictionary[sectionIndexTitle][indexPath.row];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.sectionIndexTitles;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.sectionIndexTitles count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.sectionIndexTitles[section];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    view.tintColor = GRAYCOLOR;
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:GREENCOLOR];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *sectionIndexTitle = self.sectionIndexTitles[section];
    return [self.alphabetizedDictionary[sectionIndexTitle] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CITY_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    cell.backgroundColor = GRAYCOLOR;
    cell.textLabel.textColor = GREENCOLOR;
    City *city = [self objectAtIndexPath:indexPath];
    cell.textLabel.text = city.name;
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:FromSelectCityToFilmsSegue]) {
        City *city = [self objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        self.filter.city = city;
    }
}
@end
