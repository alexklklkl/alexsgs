//
//  SelectCityTableViewController.h
//  alex
//
//  Created by Alex on 02.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"
#import "Cities.h"
#import "Constants.h"
#import "FilmsTableViewController.h"
#import "Alphabetizer.h"

@interface SelectCityTableViewController : UITableViewController

@property (strong, nonatomic) Cities *cities;
@property (nonatomic, strong) Filter *filter;
@property (nonatomic) NSDictionary *alphabetizedDictionary;
@property (nonatomic) NSArray *sectionIndexTitles;

- (City *)objectAtIndexPath:(NSIndexPath *)indexPath;
@end
