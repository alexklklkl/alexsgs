//
//  DetailViewController.h
//  alex
//
//  Created by Alex on 06.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Helper.h"
#import "Film.h"
#import "Theaters.h"
#import "RemoteSchedules.h"
#import "Filter.h"
#import "Schedule.h"
#import "MapViewController.h"

@interface DetailViewController : UIViewController <RemoteSchedulesDelegate>

@property (nonatomic, strong) Film *film;
@property (nonatomic, assign) Theaters *theaters;
@property (nonatomic, strong) RemoteSchedules *remoteSchedules;
@property (nonatomic, strong) Filter *filter;
@property (nonatomic, strong) NSString *idTheater;
@property (nonatomic, strong) NSMutableArray *schedulesArray;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (weak, nonatomic) IBOutlet UILabel *directorLabel;
@property (weak, nonatomic) IBOutlet UILabel *rolesLabel;
@property (weak, nonatomic) IBOutlet UILabel *descrLabel;
@property (weak, nonatomic) IBOutlet UIView *scheduleView;
@property (weak, nonatomic) IBOutlet UILabel *captionScheduleLabel;

- (IBAction)backButtonClicked:(UIButton *)sender;
@end
