//
//  MapViewController.m
//  alex
//
//  Created by Alex on 11.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initInterface];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) initInterface {
    self.mapView.mapType = MKMapTypeStandard;
    [self zoomToSelectedTheater];
    [self addPins];
}

-(void) zoomToSelectedTheater {
    CGFloat span = [self.theaters getSpan];
    Theater *theater = [self.theaters getTheaterWithId:self.idTheater];
    MKCoordinateRegion region;
    region.center.latitude = theater.latitude;
    region.center.longitude = theater.longitude;
    region.span.latitudeDelta = span;
    region.span.longitudeDelta = span;
    [self.mapView setRegion:region];
}

-(void) addPins {
    for (Theater *theater in [self.theaters getTheaters]) {
        CLLocationCoordinate2D  coords;
        coords.latitude = theater.latitude;
        coords.longitude = theater.longitude;
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = coords;
        point.title = theater.name;
        point.subtitle = [NSString stringWithFormat:@"%@, %@", theater.adress, theater.phone];
        [self.mapView addAnnotation:point];
    }
}

- (IBAction)backButtonClicked:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
