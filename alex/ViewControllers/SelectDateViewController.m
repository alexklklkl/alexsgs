//
//  SelectDateViewController.m
//  alex
//
//  Created by Alex on 03.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "SelectDateViewController.h"
#import "FilmsTableViewController.h"


@implementation SelectDateViewController

NSInteger captionHeight = 50;
NSInteger buttonHeight = 42;
NSInteger width = 250;
NSInteger height = 0;


-(SelectDateViewController*) initWithDelegate:(id <SelectDateViewControllerDelegate>) delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}

-(void)show {
    self.overlayView = [[UIControl alloc] init];
    CGRect screenRect = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    self.overlayView.frame = screenRect;
    self.overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [self.overlayView setUserInteractionEnabled:YES];
    [self.overlayView addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    [[[UIApplication sharedApplication] keyWindow] addSubview:self.overlayView];
//    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview: self.overlayView];
    
    height = (captionHeight + buttonHeight * 7);
    width = MIN(width, screenRect.size.width);
    height = MIN(height, screenRect.size.height);
    NSInteger marginX = (screenRect.size.width - width) / 2.0;
    NSInteger marginY = (screenRect.size.height - height) / 2.0;
    self.view = [[UIView alloc] init];
    self.view.backgroundColor = GRAYCOLOR;
    self.view.frame = CGRectMake(marginX, marginY, width, height);
    self.view.alpha = 0;
    [self.overlayView addSubview:self.view];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, captionHeight)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = NSLocalizedString(@"Выберите день", nil);
    [self.view addSubview:label];
    
    NSDate *date = [NSDate date];
    for (NSInteger i = 0; i < 7; i++) {
        NSString *title = [self.filter getDateNameForDate:date];
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, captionHeight + buttonHeight * i, width, buttonHeight)];
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitleColor:GREENCOLOR forState:UIControlStateNormal];
        button.tag = i;
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        date = [date dateByAddingTimeInterval: 86400];
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.view.alpha = 1;
    }];
}

-(void) hide {
    [self.overlayView removeTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    [UIView animateWithDuration:0.3 animations:^{
        self.overlayView.alpha = 0;
    } completion:^(BOOL finished){
        [self.overlayView removeFromSuperview];
    }];
}

-(void) buttonClick:(UIButton*)sender {
    NSInteger dayOffset = sender.tag;
    self.filter.date = [[NSDate date] dateByAddingTimeInterval: (86400 * dayOffset)];
    [self hide];
    [self.delegate wasSelectedDateWithFilter:self.filter];
}

@end
