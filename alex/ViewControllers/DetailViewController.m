//
//  DetailViewController.m
//  Отображение детальной информации о фильме
//
//  Created by Alex on 06.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initInterface];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)hasError:(NSError*)error {
    
}

-(void) initInterface {
    UIView *prevView = self.imageView;
    self.captionScheduleLabel.hidden = YES;
    self.captionScheduleLabel.text = [NSString stringWithFormat:@"Сеансы на %@", [[self.filter getDateNameForFilterDate] lowercaseString]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *imagePath = [[Helper getImagePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", self.film.idFilm]];

    // Детальная картинка
    // TODO - сделать слайдер для нескольких картинок и ссылкой на трейлер
    if ([fileManager fileExistsAtPath:imagePath]) {
        [self.imageView setImage:[UIImage imageWithContentsOfFile:imagePath]];
    }
    
    // Если соответствующее свойство непустое, инициализируем IULabel с верными NSLayoutConstraint (top относительно bottom предыдущего элемента), иначе удаляем UILabel из иерархии
    if ([self.film.name length] > 0) {
        self.captionLabel.text = self.film.name;
        [self.scrollView addConstraint: [NSLayoutConstraint constraintWithItem:self.captionLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:MARGIN * 2]];
        prevView = self.captionLabel;
    } else {
        [self.captionLabel removeFromSuperview];
    }
    if ([self.film.country length] > 0) {
        self.countryLabel.text = [NSString stringWithFormat:@"%@, %@, %@", self.film.country, self.film.year, self.film.length ];
        [self.scrollView addConstraint: [NSLayoutConstraint constraintWithItem:self.countryLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:MARGIN]];
        prevView = self.countryLabel;
    } else {
        [self.countryLabel removeFromSuperview];
    }
    if ([self.film.genries length] > 0) {
        self.genreLabel.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Жанр", nil), self.film.genries];
        [self.scrollView addConstraint: [NSLayoutConstraint constraintWithItem:self.genreLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:MARGIN]];
        prevView = self.genreLabel;
    } else {
        [self.genreLabel removeFromSuperview];
    }
    if ([self.film.director length] > 0) {
        self.directorLabel.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Режиссер", nil), self.film.director];
        [self.scrollView addConstraint: [NSLayoutConstraint constraintWithItem:self.directorLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:MARGIN]];
        prevView = self.directorLabel;
    } else {
        [self.directorLabel removeFromSuperview];
    }
    if ([self.film.roles length] > 0) {
        self.rolesLabel.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"В ролях", nil), self.film.roles];
        [self.scrollView addConstraint: [NSLayoutConstraint constraintWithItem:self.rolesLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:MARGIN]];
        prevView = self.rolesLabel;
    } else {
        [self.rolesLabel removeFromSuperview];
    }
    if ([self.film.descr length] > 0) {
        self.descrLabel.text = self.film.descr;
        [self.scrollView addConstraint: [NSLayoutConstraint constraintWithItem:self.descrLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:MARGIN]];
        prevView = self.descrLabel;
    } else {
        [self.descrLabel removeFromSuperview];
    }
    [self.scrollView addConstraint: [NSLayoutConstraint constraintWithItem:self.scheduleView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:MARGIN * 4]];
    
}

// Отправляем запрос на расписание этого фильма, по готовности - readySchedules
-(void)initData {
    self.remoteSchedules = [[RemoteSchedules alloc] initWithDelegate:self andTeathers:self.theaters];
    [self.remoteSchedules initSchedulesWithFilter:self.filter];
}

// Получаем массив кинотеатров с массивом разписаний в каждом из них
-(void)readySchedules:(NSMutableArray *)schedules {
    [self.navigationController.view makeToast:[NSString stringWithFormat:@"Расписание готово"] andType:TOASTINFO];
    self.schedulesArray = schedules;
    self.captionScheduleLabel.hidden = NO;
    
    // Следующий элемент выводится ниже предыдущего с использованием NSLayoutConstraint
    UIView *prevView = self.captionScheduleLabel;
    
    for (Schedule *schedule in schedules) {
        // Название кинотеатра
        prevView = [self setCaptionWithText:schedule.theater.name afterView:prevView];
        // Кнопка для перехода на карту
        [self setLocationButtonWithTheater:schedule.theater afterView:prevView];
        // Адрес кинотеатра
        prevView = [self setAdressWithText:schedule.theater.adress afterView:prevView];
        // Расписание
        prevView = [self setSchedule:schedule.schedules afterView:prevView];
    }
    
    //NSLayoutConstraint для контейнера
    [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:self.scheduleView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.f]];
}

// Кнопка для перехода на карту
- (void)setLocationButtonWithTheater:(Theater*)theater afterView:(UIView*)prevView {
    if ((theater.latitude > 0) && (theater.longitude > 0)) {
        UIButton *locationButton = [[UIButton alloc] init];
        locationButton.translatesAutoresizingMaskIntoConstraints = NO;
        [locationButton setTitle:@"" forState:UIControlStateNormal];
        [locationButton setImage:[UIImage imageNamed:@"location.png"] forState:UIControlStateNormal];
        locationButton.tag = [theater.idTheater intValue];
        [self.scheduleView addSubview:locationButton];
        [locationButton addTarget:self
                       action:@selector(locationButtonClicked:)
             forControlEvents: UIControlEventTouchUpInside];
        
        [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:locationButton attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.scheduleView attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:0.f]];
        [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:locationButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeTop multiplier:1.0f constant:5]];
        [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:locationButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.scheduleView attribute:NSLayoutAttributeWidth multiplier:0.08f constant:0.f]];
        [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:locationButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.scheduleView attribute:NSLayoutAttributeWidth multiplier:0.08f constant:0.f]];
        
    }
}

// Название кинотеатра
-(UIView*)setCaptionWithText:(NSString*)text afterView:(UIView*)prevView {
    UILabel *caption = [[UILabel alloc] init];
    caption.translatesAutoresizingMaskIntoConstraints = NO;
    caption.textColor = [UIColor whiteColor];
    caption.numberOfLines = 0;
    caption.font = [UIFont boldSystemFontOfSize:26];
    caption.text = text;
    [self.scheduleView addSubview:caption];
    
    [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:caption attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.scheduleView attribute:NSLayoutAttributeLeading multiplier:1.0f constant:0.f]];
    [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:caption attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:BEFORE_CAPTION_MARGIN]];
    [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:caption attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.scheduleView attribute:NSLayoutAttributeWidth multiplier:0.85f constant:0.f]];
    
    return caption;
}

// Адрес кинотеатра
-(UIView*)setAdressWithText:(NSString*)text afterView:(UIView*)prevView {
    UILabel *adress = [[UILabel alloc] init];
    adress.translatesAutoresizingMaskIntoConstraints = NO;
    adress.textColor = [UIColor grayColor];
    adress.numberOfLines = 0;
    adress.text = text;
    [self.scheduleView addSubview:adress];
    
    [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:adress attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.scheduleView attribute:NSLayoutAttributeLeading multiplier:1.0f constant:0.f]];
    [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:adress attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0]];
    [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:adress attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.scheduleView attribute:NSLayoutAttributeWidth multiplier:0.85f constant:0.f]];
    return adress;
}

// Расписание
// TODO - перейти от NSLayoutConstraint к обычным frame, должно быстрее отображаться. Если будет неудовлетворительно - UIWebView с контентом, формируемом не в UI-потоке
-(UIView*)setSchedule:(NSMutableArray*)schedules afterView:(UIView*)prevView {
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    // Число элементов расписания в строке
    NSInteger scheduleCountInRow = SCHEDULE_COUNT_IN_ROW;
    if (screenWidth <= SMALL_SCREEN) { // малые экраны
        scheduleCountInRow--;
    } else if (screenWidth >= LARGE_SCREEN) { // планшеты
        scheduleCountInRow += 2;
    }
    UILabel *label;
    // Ширина и поля каждого элемента расписания
    CGFloat viewWidth = self.scheduleView.frame.size.width;
    CGFloat margin = viewWidth * 1.0 / (scheduleCountInRow - 1 + scheduleCountInRow * SCHEDULE_RATIO_WIDTH_MARGIN);
    CGFloat width = margin * SCHEDULE_RATIO_WIDTH_MARGIN;
    //Количество строк с элементами расписания
    NSInteger countRows = ceil(1.0 * [schedules count] / scheduleCountInRow);
    for (NSInteger i = 0; i < countRows; i++) {
        NSInteger countItemsInLine;
        NSInteger needToDraw = [schedules count] - i * scheduleCountInRow;
        // Число элементов расписания в текущей строке
        if (needToDraw >= scheduleCountInRow)
            countItemsInLine = scheduleCountInRow;
        else
            countItemsInLine = needToDraw;
        //добавление элементов расписания с NSLayoutConstraint
        for (NSInteger j = 0; j < countItemsInLine; j++) {
            label = [[UILabel alloc] init];
            label.translatesAutoresizingMaskIntoConstraints = NO;
            label.textColor = GREENCOLOR;
            label.textAlignment = NSTextAlignmentCenter;
            label.text = [schedules objectAtIndex:i * scheduleCountInRow + j];
            label.layer.masksToBounds = YES;
            label.layer.cornerRadius = SCHEDULE_CORNER_RADIUS;
            label.layer.borderColor = [UIColor grayColor].CGColor;
            label.layer.borderWidth = 1.0;
            [self.scheduleView addSubview:label];
            
            
            CGFloat leading = (margin + width) * j;
            [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.scheduleView attribute:NSLayoutAttributeLeading multiplier:1.0f constant:leading]];
            [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:prevView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:margin * 0.5]];
            [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:0.85f constant:width]];
            [self.scheduleView addConstraint: [NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:0.85f constant:width / SCHEDULE_RATIO_WIDTH_HEIGHT]];
        }
        prevView = label;
    }
    return prevView;
}

#pragma mark Events

// Нажата кнопка перехода на карту
-(void) locationButtonClicked:(UIButton*)button {
    self.idTheater = [NSString stringWithFormat:@"%ld", (long)button.tag];
    [self performSegueWithIdentifier:FromDetailToMapSegue sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:FromDetailToMapSegue]) {
        MapViewController *mapViewController = segue.destinationViewController;
        mapViewController.theaters = self.theaters;
        mapViewController.idTheater = self.idTheater;
    }
}

#pragma mark Other

- (IBAction)backButtonClicked:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
