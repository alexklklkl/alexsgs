//
//  MainTableViewController.h
//  alex
//
//  Created by Alex on 16.11.16.
//  Copyright © 2016 kode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "City.h"
#import "Cities.h"
#import "Genre.h"
#import "Genries.h"
#import "Theater.h"
#import "Theaters.h"
#import "Film.h"
#import "Films.h"
#import "FilmTableViewCell.h"
#import "SelectCityTableViewController.h"
#import "FilterGenriesTableViewController.h"
#import "SelectDateViewController.h"
#import "DetailViewController.h"
#import "CurrentCity.h"
#import "Splash.h"


@class KCFloatingActionButton;
@class KCFloatingActionButtonItem;

@protocol FilmsTableViewController <NSObject>
-(void) readyCities;
-(void) readyGenries;
-(void) readyFilms;
-(void) readyFilmImage:(Film *)film;
-(void) readyFilm:(Film *)film;
-(void) hasError:(NSError*)error;
@end


@interface FilmsTableViewController : UITableViewController <TheatersDelegate, FilmsDelegate, GenriesDelegate, CitiesDelegate, SelectDateViewControllerDelegate, CurrentCityDelegate, SplashDelegate>
@property (strong, nonatomic) Cities *cities;
@property (strong, nonatomic) Genries *genries;
@property (strong, nonatomic) Films *films;
@property (strong, nonatomic) Theaters *theaters;
@property (strong, nonatomic) CurrentCity *currentCity;
@property (strong, nonatomic) Splash *splash;
@property (strong, nonatomic) KCFloatingActionButton *actionButton;
@property (strong, nonatomic) KCFloatingActionButtonItem *genriesItemActionButton;
@property (strong, nonatomic) KCFloatingActionButtonItem *ratingItemActionButton;
@property (strong, nonatomic) KCFloatingActionButtonItem *dateItemActionButton;
@property (strong, nonatomic) KCFloatingActionButtonItem *cityItemActionButton;
@property (strong, nonatomic) SelectDateViewController *selectDateViewController;

- (IBAction)unwindFromSelectCityViewController: (UIStoryboardSegue *)segue;
- (IBAction)unwindFromFilterGenriesViewController: (UIStoryboardSegue *)segue;
-(void) wasSelectedDateWithFilter:(Filter*)filter;
@end
