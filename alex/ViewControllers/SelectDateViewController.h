//
//  SelectDateViewController.h
//  alex
//
//  Created by Alex on 03.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Filter.h"

@protocol SelectDateViewControllerDelegate <NSObject>
-(void) wasSelectedDateWithFilter:(Filter*)filter;
@end

@class FilmsTableViewController;

@interface SelectDateViewController : NSObject
@property (strong, nonatomic) UIControl *overlayView;
@property (strong, nonatomic) UIView *view;
@property (strong, nonatomic) FilmsTableViewController *filmsTableViewController;
@property (strong, nonatomic) Filter *filter;
@property (nonatomic, assign) id <SelectDateViewControllerDelegate> delegate;

-(SelectDateViewController*) initWithDelegate:(id <SelectDateViewControllerDelegate>) delegate;
-(void) show;
-(void) hide;
@end
