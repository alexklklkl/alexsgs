//
//  MainTableViewController.m
//
//  Корневой viewController
//  Отображает в UItableView список фильмов с текущим фильтром
//  Меню в виде ActionButton
//  Выбор города, даты, сортировки по рейтингу/названию фильма и фильтр по жанрам
//  Переход к DetailViewController (детальная информация о фильма)
//
//  Делегат:
//  TheatersDelegate (готов список кинотеатров),
//  FilmsDelegate (готов список фильмов),
//  GenriesDelegate (готов список жанров),
//  CitiesDelegate (готов список городов),
//  SelectDateViewControllerDelegate (выбрана новая дата),
//  CurrentCityDelegate (выбран новый город),
//  SplashDelegate (скрыта заставка)
//
//
//  Copyright © 2017 kode. All rights reserved.
//

#import "FilmsTableViewController.h"
#import "alex-Swift.h"

@interface FilmsTableViewController ()
@end

@implementation FilmsTableViewController

#pragma mark - init
- (void)viewDidLoad {
    [super viewDidLoad];
    [self showSplash];
    
    [self initFiles];
    [self initInterface];
    [self initData];
}

-(void) initFiles {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    //Каталог для картинок
    NSString *imagesDirName = [Helper getImagePath];
    if (![fileManager fileExistsAtPath:imagesDirName])
        [fileManager createDirectoryAtPath:imagesDirName withIntermediateDirectories:YES attributes:nil error:&error];
    
    //Скопируем базу данных в каталог documents
    NSString *dbFileName = [Helper getDBFile];
    if (![fileManager fileExistsAtPath:dbFileName]) {
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"common" ofType:@"db"];
        [fileManager copyItemAtPath:resourcePath toPath:dbFileName error:&error];
    }
}

- (void)initInterface {
    self.title = @"";
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100; // Неважно, перегрузится...
    [self initActionButton];
}


-(void)initData {
    // TODO сделать проверку наличия интернета
    
    // Создадим модели
    self.cities = [[Cities alloc] initWithDelegate:self];
    self.genries = [[Genries alloc] initWithDelegate:self];
    self.theaters = [[Theaters alloc] initWithDelegate:self];
    self.films = [[Films alloc] initWithDelegate:self];
    
    // Определить город по GPS? или можно взять из NSUserDefaults?
    if (ALWAYS_DETECT_CITY_VIA_GPS) {
        // По GPS, делегаты - readyCurrentCity или noCurrentCity
        self.currentCity = [[CurrentCity alloc] init];
        [self.currentCity initCurrentCityWithDelegate:self andCities:self.cities];
    } else {
        City *city = [Filter getStoredCity];
        if (city) { // Город из NSUserDefaults
            [self readyCurrentCity:city andMessage:[NSString stringWithFormat:@"Предыдущий город %@", city.name]];
        } else { // Город по GPS, делегаты - readyCurrentCity или noCurrentCity
            self.currentCity = [[CurrentCity alloc] init];
            [self.currentCity initCurrentCityWithDelegate:self andCities:self.cities];
        }
    }

    // Справичники городов и жанров, делегаты readyCities и readyGenries
    // Данные сначала "поищутся" в базе, если нет - запросятся через API
    [self.cities initCities];
    [self.genries initGenries];

    // Фильмы и кинотеатры запросим позже, когда узнаем город и заполним фильтр
    //[self.theaters initTheatersWithFilter:filter];
    //[self.films initFilmsWithFilter:filter];
}

-(void)initActionButton {
    // Кнопки Action button
    self.actionButton = [[KCFloatingActionButton alloc] init];
    self.actionButton.buttonColor = GREENCOLOR;
    self.actionButton.buttonImage = [UIImage imageNamed:@"fab.png"];
    
    __weak typeof(self) weakSelf = self;
    self.genriesItemActionButton = [self.actionButton addItem:NSLocalizedString(@"Фильтр по жанрам", nil) icon:[UIImage imageNamed:@"filter.png"] handler:^(KCFloatingActionButtonItem *item) {
        [weakSelf genriesClicked];
        [weakSelf closeFab];
    }];
    self.genriesItemActionButton.hidden = YES;
    self.ratingItemActionButton = [self.actionButton addItem:NSLocalizedString(@"По рейтингу", nil) icon:[UIImage imageNamed:@"order.png"] handler:^(KCFloatingActionButtonItem *item) {
        [weakSelf ratingClicked];
        [weakSelf performSelector:@selector(closeFab) withObject:nil afterDelay:CLOSEFABDELAY];
    }];
    //self.ratingItemActionButton.hidden = YES; Всегда видим
    self.cityItemActionButton = [self.actionButton addItem:@"" icon:[UIImage imageNamed:@"city.png"] handler:^(KCFloatingActionButtonItem *item) {
        [weakSelf cityClicked];
        [weakSelf closeFab];
    }];
    self.cityItemActionButton.hidden = YES;
    self.dateItemActionButton = [self.actionButton addItem:NSLocalizedString(@"Сегодня", nil) icon:[UIImage imageNamed:@"calendar.png"] handler:^(KCFloatingActionButtonItem *item) {
        [weakSelf dateClicked];
        [weakSelf closeFab];
    }];
    //self.dateItemActionButton.hidden = YES; Всегда видим
    
    //Когда нужно, отобразим с анимацией
    [self.actionButton setHidden:true];
    [self.navigationController.view addSubview:self.actionButton];
}


-(void)closeFab {
    [self.actionButton close];
}

#pragma mark - Menu actions

// ---------------------------- Menu genries ------------------------------
// Выбран фильтр по жанрам
-(void) genriesClicked {
    [self performSegueWithIdentifier:FromFilmsToFilterGenriesSegue sender:self];
}

// Передадим копию фильтра, он вернется с выбранными жанрами
-(void)prepareGenries:(UIStoryboardSegue *)segue {
    FilterGenriesTableViewController *filterGenriesTableViewController = segue.destinationViewController;
    filterGenriesTableViewController.genries = self.genries;
    filterGenriesTableViewController.filter = [self.films.filter getFilterCopy];
}

// Выбраны (или не выбраны) жанры, обновим данные с новым фильтром
- (IBAction)unwindFromFilterGenriesViewController: (UIStoryboardSegue *)segue {
    FilterGenriesTableViewController *filterGenriesTableViewController = (FilterGenriesTableViewController *)[segue sourceViewController];
    Filter *filter = filterGenriesTableViewController.filter;
    // "Главный" фильтр в Films - self.films
    [self.films initFilmsWithFilter:filter];
}

// ---------------------------- Menu rating ------------------------------
// Сортировка по рейтингу/имени
-(void) ratingClicked {
    Filter *filter = self.films.filter;
    // Инвертируем порядок сортировки
    if ([filter.order  isEqual: ORDERRATING]) {
        filter.order = ORDERNAME;
        [self.ratingItemActionButton setTitle:NSLocalizedString(@"По имени", nil)];
    } else {
        filter.order = ORDERRATING;
        [self.ratingItemActionButton setTitle:NSLocalizedString(@"По рейтингу", nil)];
    }
    // Обновим фильмы с новым фильтром
    [self.films initFilmsWithFilter:filter];
}

// ---------------------------- Menu date ------------------------------
// Выбор даты
-(void) dateClicked {
    // SelectDateViewController - это не UIViewController, а NSObject
    if (!self.selectDateViewController) {
        self.selectDateViewController = [[SelectDateViewController alloc] initWithDelegate:self];
    }
    // Передадим копию фильтра
    self.selectDateViewController.filter = [self.films.filter getFilterCopy];
    // При выборе новой даты вызовется wasSelectedDateWithFilter
    [self.selectDateViewController show];
}

// Делегат выбора даты
-(void) wasSelectedDateWithFilter:(Filter*)filter {
    // Фильтр с новой датой, обновим фильмы и текст кнопки
    [self.films initFilmsWithFilter:filter];
    [self.dateItemActionButton setTitle:[self.films.filter getDateNameForFilterDate]];
}

// ---------------------------- Menu city ------------------------------
// Ручной выбор города
-(void) cityClicked {
    [self performSegueWithIdentifier:FromFilmsToSelectCitySegue sender:self];
}

-(void) prepareCities:(UIStoryboardSegue *)segue {
    SelectCityTableViewController *selectCityTableViewController = segue.destinationViewController;
    // Передадим список городов и копию фильтра
    selectCityTableViewController.cities = self.cities;
    selectCityTableViewController.filter = [self.films.filter getFilterCopy];
}

- (IBAction)unwindFromSelectCityViewController: (UIStoryboardSegue *)segue {
    SelectCityTableViewController *selectCityTableViewController = (SelectCityTableViewController*)[segue sourceViewController];
    Filter* filter = selectCityTableViewController.filter;
    // Новый город в NSDefaults
    [filter storeCity];
    // Список кинотеатров для нового города
    [self.theaters initTheatersWithFilter:filter];
    // Обновим фильмы для нового города
    [self.films initFilmsWithFilter:filter];
    // Надпись для кнопки города - текущий город
    [self.cityItemActionButton setTitle:[self.films.filter getCityName]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:FromFilmsToSelectCitySegue]) {
        //Выбор города
        [self prepareCities:segue];
    } else if ([segue.identifier isEqualToString:FromFilmsToFilterGenriesSegue]) {
        //Фильтр жанров
        [self prepareGenries:segue];
    } else if ([segue.identifier isEqualToString:FromFilmsToDetailSegue]) {
        //Детальный
        [self prepareDetail:segue];
    }
}

#pragma mark - Detail
// Детальнный viewController с информацией о выбранном фильме
-(void) prepareDetail:(UIStoryboardSegue *)segue {
    DetailViewController *detailViewController = segue.destinationViewController;
    NSInteger index = [self.tableView indexPathForSelectedRow].row;
    Film *film = [[self.films getFilms] objectAtIndex:index];
    // Передадим фильм
    detailViewController.film = film;
    self.films.filter.idFilm = film.idFilm;
    // Можно не копию фильтра, модифицироваться не будет
    detailViewController.filter = self.films.filter;
    // Кинотеатры для расписания
    detailViewController.theaters = self.theaters;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - delegates

// Определились с текущим городом
-(void)readyCurrentCity:(City *)city andMessage:(NSString *)message {
    [self.navigationController.view makeToast:message andType:TOASTINFO];
    Filter *filter = [[Filter alloc] init];
    // Выбранный город в фильтр по умолчанию
    filter.city = city;
    
    [self useCityInFilter:filter];
}

// Не можем определить текущий город, установим город по умолчанию
-(void)noCurrentCity {
    //TODO - выбор города
    [self.navigationController.view makeToast:[NSString stringWithFormat:@"Город по умолчанию"] andType:TOASTINFO];
    Filter *filter = [[Filter alloc] init];
    // Фильтр с городом по умолчанию
    [filter setDefaults];
    
    [self useCityInFilter:filter];
}

-(void)useCityInFilter:(Filter*) filter {
    // Сохраним город в NSUserDefaults
    [filter storeCity];
    
    // Обновим кинотеатры и фильмы для нового города
    [self.theaters initTheatersWithFilter:filter];
    [self.films initFilmsWithFilter:filter];
    
    // Надпись на кнопке выбора города
    [self.cityItemActionButton setTitle:[filter getCityName]];
}

-(void) readyCities {
// Поступил список городов, можно выбирать город
// TODO - дождаться окончания загрузки дельной информации по всем фильмам
    self.cityItemActionButton.hidden = NO;
}

-(void) readyGenries {
    // Поступил список жанров, можно фильтровать фильмы по жанрам
    self.genriesItemActionButton.hidden = NO;
}

-(void) readyTheaters {
// TODO Пока для отладки
    [self.navigationController.view makeToast:@"Поступили кинотеатры" andType:TOASTINFO];
}

-(void)readyFilms {
// Готов список фильмов с неполными данными (только id и name), обновим всю таблицу
    [self.tableView reloadData];
    [self.splash hide];
    //[self.theaters initTheatersWithFilter:self.films.filter];
}

-(void)readyFilm:(Film *)film {
    //[self.tableView reloadData];
// Готовы детальная информация о фильме, обновим одну ячейку
    NSInteger index = [self.films getFilmIndexById:film.idFilm];
    if (index != -1) {
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(void)readyFilmImage:(Film *)film {
// Загружена и обработана картинка фильма, обновим одну ячейку
    NSInteger index = [self.films getFilmIndexById:film.idFilm];
    if (index != -1) {
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    }
}

-(void)hasError:(NSError*)error {
// Тост с сообщением об ошибке
// TODO Попросить пользователя проверить интернет
    [self.navigationController.view makeToast:error.localizedDescription andType:TOASTERROR];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = [[self.films getFilms] count];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FilmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:FILM_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    Film *film = [self.films getFilmByIndex:indexPath.row];
    if (film) {
        [cell setContentForFilm:film];
    }
    return cell;
}

#pragma mark Other

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
// покажем actionButton с анимацией
    [self.actionButton animateShow:true];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
// скроем actionButton с анимацией
    [self.actionButton animateShow:false];
}

// Анимированный сплэш пока загружается список фильмов, скроем в readyFilms
-(void)showSplash {
    self.splash = [[Splash alloc] initWithDelegate:self];
    [self.navigationController.view addSubview:self.splash];
    [self.splash animate];
}

// Почистим сплэш после всех анимаций, больше не нужен
-(void)removeSplash {
    [self.splash removeFromSuperview];
    self.splash = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// Можно скрывать actionButton при скролле и отображать только в начале и конце таблицы
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    NSInteger bottomScroll = scrollView.contentSize.height - scrollView.frame.size.height - SCROLLTOHIDEFAB;
//    if (
//        (scrollView.contentOffset.y < SCROLLTOHIDEFAB) ||
//        (scrollView.contentOffset.y > bottomScroll)
//        ) {
//        if (!self.actionButton.isVisible)
//            [self.actionButton animateShow:true];
//    } else {
//        if (self.actionButton.isVisible)
//            [self.actionButton animateShow:false];
//    }
//}

@end
