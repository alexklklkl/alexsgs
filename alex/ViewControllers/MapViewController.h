//
//  MapViewController.h
//  alex
//
//  Created by Alex on 11.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Theaters.h"


@interface MapViewController : UIViewController <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (nonatomic, strong) Theaters *theaters;
@property (nonatomic, strong) NSString *idTheater;

- (IBAction)backButtonClicked:(UIButton *)sender;
@end
