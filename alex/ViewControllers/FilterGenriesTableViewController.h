//
//  FilterGenriesTableViewController.h
//  alex
//
//  Created by Alex on 03.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "FilmsTableViewController.h"
#import "Genre.h"
#import "Genries.h"
#import "GenreTableViewCell.h"
#import "Filter.h"

@interface FilterGenriesTableViewController : UITableViewController <GenreTableViewCellDelegate>

@property (nonatomic, strong) Genries *genries;
@property (nonatomic, strong) Filter *filter;

@end
