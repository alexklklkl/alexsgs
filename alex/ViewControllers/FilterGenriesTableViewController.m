//
//  FilterGenriesTableViewController.m
//  alex
//
//  Created by Alex on 03.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "FilterGenriesTableViewController.h"

@interface FilterGenriesTableViewController ()

@end

@implementation FilterGenriesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Фильтр", nil);
    self.view.backgroundColor = GRAYCOLOR;
    self.tableView.sectionIndexBackgroundColor = [UIColor blackColor];
    self.tableView.sectionIndexColor = GREENCOLOR;
    self.navigationController.navigationBar.barTintColor = GRAYCOLOR;
    self.navigationController.navigationBar.tintColor = GREENCOLOR;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationItem.rightBarButtonItem setTitle:@"Применить"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.genries getGenries] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GenreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:GENRE_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    if (cell.delegate == nil) {
        //[cell setDelegate:self];
        cell.delegate = self;
    }
    BOOL checked = false;
    Genre *genre = [[self.genries getGenries] objectAtIndex:indexPath.row];
    if ([self.filter indexGenreInFilter:genre] == -1) {
        checked = false;
    } else {
        checked = true;
    }
    [cell setContentForGenre:genre andChecked:checked andIndex:indexPath.row];
    return cell;
}

-(void)genreWasSetTo:(BOOL)checked withIndex:(NSInteger)index {
    if (checked) {
        [self.filter addGenreToFilter:[[self.genries getGenries] objectAtIndex:index]];
    } else {
        [self.filter removeGenreFromFilter:[[self.genries getGenries] objectAtIndex:index]];
    }
}

@end
