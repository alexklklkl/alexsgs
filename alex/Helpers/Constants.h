//
//  Constants.h
//  alex
//
//  Created by Alex on 17.11.16.
//  Copyright © 2016 kode. All rights reserved.
//

#define DEBUG_MODE
#define CLEAR_IMAGES_EVERY_RUN false
#define ALWAYS_DETECT_CITY_VIA_GPS true

#define DBPATH @""
#define IMAGEPATH @"/images"
#define TRACEDBEXECUTION false
#define DBNAME @"common.db"

#define TOASTERROR 1
#define TOASTINFO 2

//Segues
#define FromFilmsToSelectCitySegue @"FromFilmsToSelectCity"
#define FromSelectCityToFilmsSegue @"FromSelectCityToFilms"
#define FromFilmsToFilterGenriesSegue @"FromFilmsToFilterGenries"
#define FromFilterGenriesToFilmsSegue @"FromFilterGenriesToFilms"
#define FromFilmsToDetailSegue @"FromFilmsToDetail"
#define FromDetailToFilmsSegue @"FromDetailToFilms"
#define FromDetailToMapSegue @"FromDetailToMap"

//Reuse identifiers
#define FILM_CELL_REUSE_IDENTIFIER @"FilmCell"
#define CITY_CELL_REUSE_IDENTIFIER @"CityCell"
#define GENRE_CELL_REUSE_IDENTIFIER @"GenreCell"

//Interface
#define BEFORE_CAPTION_MARGIN 24
#define SCHEDULE_COUNT_IN_ROW 5
#define MARGIN 8
#define SCHEDULE_RATIO_WIDTH_MARGIN 2.8
#define SCHEDULE_RATIO_WIDTH_HEIGHT 2.0
#define SCHEDULE_CORNER_RADIUS 6.0
#define CELL_RATIO 0.5f
#define ASPECT_RATIO -1
#define IMAGEALPHA 0.7f
#define CLOSEFABDELAY 0.5f
#define SCROLLTOHIDEFAB 300
#define SMALL_SCREEN 320
#define LARGE_SCREEN 768
#define JPG_QUALITY 0.7

//Colors
#define GREENCOLOR [UIColor colorWithRed:101/255.0 green:191/255.0 blue:166/255.0 alpha:1.0f]
#define GRAYCOLOR [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0f]


//Таблицы
#define CITIESTABLENAME @"cities"
#define GENRIESTABLENAME @"genries"
#define THEATERSTABLENAME @"theaters"
#define FILMSTABLENAME @"films"

//Поля
#define IDCITY @"idCity"
#define IDGENRE @"idGenre"
#define IDTHEATER @"idTheater"
#define IDFILM @"idFilm"
#define NAME @"name"
#define YEAR @"year"
#define LENGTH @"length"
#define RATING @"rating"
#define COUNTRY @"country"
#define AGE @"age"
#define DIRECTOR @"director"
#define GENRIES @"genries"
#define ROLES @"roles"
#define DESCR @"descr"
#define TRAILER @"trailer"
#define ADRESS @"adress"
#define PHONE @"phone"
#define LATITUDE @"latitude"
#define LONGITUDE @"longitude"
#define SCHEDULES @"schedules"
#define RESULTS @"results"
#define ADRESS_COMPONENTS @"address_components"
#define TYPES @"types"
#define LOCALITY @"locality"
#define SHORT_NAME @"short_name"

//Other
#define ORDERNAME @" name asc "
#define ORDERRATING @" rating desc "
#define CITY_ID_KEY @"IDCITY"
#define DEFAULT_CITY_ID @"490"
#define CITY_NAME_KEY @"NAMECITY"
#define DEFAULT_CITY_NAME @"Калининград"

//API
#define APIURLCITIES @"http://grad39.ru/php/kinopoisk/getCities.php"
#define APIURLGENRIES @"http://grad39.ru/php/kinopoisk/getGenries.php"
#define APIURLTHEATERS @"http://grad39.ru/php/kinopoisk/getTheaters.php?idCity=%@"
#define APIURLFILMS @"http://grad39.ru/php/kinopoisk/getFilms.php?idCity=%@&date=%@"
#define APIURLFILM @"http://grad39.ru/php/kinopoisk/getFilmDetail.php?idFilm=%@"
#define APIURLSCHEDULES @"http://grad39.ru/php/kinopoisk/getSchedules.php?idCity=%@&idFilm=%@&date=%@"
#define APIURLCOORDINATES @"http://grad39.ru/php/kinopoisk/getCoordinates.php?idTheater=%@&cityName=%@&name=%@"
#define APIURLIMAGE @"http://grad39.ru/php/kinopoisk/getImage.php?idFilm=%@"

//Сторонние API
#define GOOGLEAPIKEY @"AIzaSyCpQHoR6N4Sh03EQaBlUOOTe0uvbgiTDCE"
#define GOOGLEAPIBACKGEOCODING @"https://maps.googleapis.com/maps/api/geocode/json?latlng=%@&language=ru&key=%@"


