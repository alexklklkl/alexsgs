#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@implementation CALayer (Additions)

- (void)setBorderColorFromUIColor:(UIColor *)color
{
    self.borderColor = color.CGColor;
}

@end
