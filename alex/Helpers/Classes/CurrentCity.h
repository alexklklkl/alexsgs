//
//  CurrentCity.h
//  alex
//
//  Created by Alex on 11.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Constants.h"
#import "Helper.h"
#import "Cities.h"
#import "City.h"

@protocol CurrentCityDelegate <NSObject>
-(void)readyCurrentCity:(City *)city andMessage:(NSString*)message;
-(void)noCurrentCity;
-(void)hasError:(NSError*)error;
@end

@interface CurrentCity : NSObject <CLLocationManagerDelegate>

@property (nonatomic, assign) id <CurrentCityDelegate> delegate;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) Cities *cities;

-(void) initCurrentCityWithDelegate:(id <CurrentCityDelegate>) delegate andCities:(Cities*)cities;

@end
