//
//  Helper.m
//  alex
//
//  Created by Alex on 10.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "Helper.h"
#import "Constants.h"

@implementation Helper

+(UIImage*) resizeImage:(UIImage*) sourceImage andRatio:(CGFloat)ratio {
    CGFloat newWidth = [[UIScreen mainScreen] bounds].size.width;
    
    if (ratio == ASPECT_RATIO)
        ratio = sourceImage.size.height / sourceImage.size.width;
    
    CGFloat yOffset = (sourceImage.size.height - sourceImage.size.width * ratio) * 0.5f;
    CGRect rect = CGRectMake(0, yOffset, sourceImage.size.width, sourceImage.size.width * ratio);
    
    CGSize newSize = CGSizeMake(newWidth, newWidth * ratio);
    CGImageRef imageRef = CGImageCreateWithImageInRect([sourceImage CGImage], rect);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1);
    [croppedImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resizedImage;
}

+(NSString*) getBasePath {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+(NSString*) getDBFile {
    return [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:DBNAME]];
}

+(NSString*) getImagePath {
    return [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", IMAGEPATH]];
}

+(NSError*) getError:(NSString *) errorCaption {
    NSLog(@"%@", errorCaption);
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:NSLocalizedString(errorCaption, nil) forKey:NSLocalizedDescriptionKey];
    return [NSError errorWithDomain:@"word" code:1 userInfo:details];
}


@end
