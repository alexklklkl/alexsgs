//
//  CurrentCity.m
//  alex
//
//  Created by Alex on 11.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "CurrentCity.h"

@implementation CurrentCity

-(void) initCurrentCityWithDelegate:(id <CurrentCityDelegate>) delegate andCities:(Cities*)cities {
    self.delegate = delegate;
    self.cities = cities;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation* location = [locations lastObject];
    if ([self.cities getCitiesCount] > 0) {
        [self.locationManager stopUpdatingLocation];
        self.locationManager = nil;
        [self performSelectorInBackground:@selector(requestCityWithLocation:) withObject:location];
    } else {
        // Города еще не загружены, подождем
    }
}

-(void) requestCityWithLocation:(CLLocation*) location {
    NSError *error;
    NSString *coordinates = [NSString stringWithFormat:@"%f,%f", location.coordinate.latitude, location.coordinate.longitude];
    NSString *stringURL = GOOGLEAPIBACKGEOCODING;
    stringURL = [NSString stringWithFormat:stringURL, coordinates, GOOGLEAPIKEY];
    NSURL *URL = [NSURL URLWithString:stringURL];
    NSString *dataString = [[NSString alloc]
                            initWithContentsOfURL:URL
                            encoding:NSUTF8StringEncoding
                            error:&error];
    if (error) {
        [self performSelectorOnMainThread:@selector(noCurrentCityForeground) withObject:nil waitUntilDone:NO];
        return;
    }
    NSDictionary *jsonCurrentCity = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    if (error) {
        [self performSelectorOnMainThread:@selector(noCurrentCityForeground) withObject:nil waitUntilDone:NO];
        return;
    }
    NSMutableArray *adresses = [[[jsonCurrentCity objectForKey:RESULTS] objectAtIndex:0] objectForKey:ADRESS_COMPONENTS];
    NSString *cityName = @"";
    for (NSDictionary *adress in adresses) {
        NSMutableArray *types = [adress objectForKey:TYPES];
        if ([[types objectAtIndex:0] isEqualToString:LOCALITY]) {
            cityName = [adress objectForKey:SHORT_NAME];
            break;
        }
    }
    if ([cityName length] > 0) {
        City *city = [self.cities getCityByName:cityName];
        if (city) {
            [self performSelectorOnMainThread:@selector(readyCurrentCityForeground:) withObject:city waitUntilDone:NO];
            return;
        }
    }
    [self performSelectorOnMainThread:@selector(noCurrentCityForeground) withObject:nil waitUntilDone:NO];
}

-(void) readyCurrentCityForeground:(City*)city {
    [self.delegate readyCurrentCity:city andMessage:[NSString stringWithFormat:@"Определен город %@", city.name]];
}

-(void) noCurrentCityForeground {
    [self.delegate noCurrentCity];
}

@end
