//
//  Helper.h
//  alex
//
//  Created by Alex on 10.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIView+Toast.h"

@interface Helper : NSObject

+(UIImage*) resizeImage:(UIImage*) sourceImage andRatio:(CGFloat)ratio;
+(NSString*) getBasePath;
+(NSString*) getDBFile;
+(NSString*) getImagePath;
+(NSError*) getError:(NSString *) errorCaption;

@end
