//
//  Data.h
//  alex
//
//  Created by Alex on 13.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "Helper.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabaseQueue.h"

@interface Data : NSObject
+(Data*)sharedInstance;
-(FMDatabaseQueue*)getQueue;

@property (nonatomic, strong) FMDatabaseQueue *queue;
@end
