//
//  Data.m
//  alex
//
//  Created by Alex on 13.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "Data.h"

@implementation Data
+ (Data*)sharedInstance {
    static Data *sharedInstance = nil;
    static dispatch_once_t onceToken;
    if (sharedInstance) return sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Data alloc] init];
        sharedInstance.queue = [FMDatabaseQueue databaseQueueWithPath:[[Helper getBasePath] stringByAppendingPathComponent:[NSString stringWithFormat:DBNAME]]];
    });
    return sharedInstance;
}

-(FMDatabaseQueue*)getQueue {
    return self.queue;
}
@end
