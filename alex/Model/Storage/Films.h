//
//  Films.h
//  alex
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "BaseStorage.h"
#import "RemoteFilms.h"
#import "Filter.h"
#import "Film.h"

@protocol FilmsDelegate <NSObject>
-(void) readyFilms;
-(void) readyFilmImage:(Film *)film;
-(void) readyFilm:(Film *)film;
-(void) hasError:(NSError*)error;
@end

@interface Films : BaseStorage <RemoteFilmsDelegate>

@property (strong, nonatomic) NSMutableArray *filmsArray;
@property (strong, nonatomic) Filter *filter;
@property (strong, nonatomic) RemoteFilms *remoteFilms;

-(void)initFilmsWithFilter:(Filter *) filter;
-(NSMutableArray *)getFilms;
-(NSInteger)getFilmIndexById:(NSString *)idFilm;
-(Film *)getFilmByIndex:(NSInteger)index;
-(Film *)getFilmById:(NSString*)idFilm;

-(id) initWithDelegate:(id <FilmsDelegate>) delegate;

@end
