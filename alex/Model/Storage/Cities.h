//
//  Cities.h
//  alex
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseStorage.h"
#import "Constants.h"
#import "City.h"
#import "RemoteCities.h"

@protocol CitiesDelegate <NSObject>
-(void) readyCities;
-(void) hasError:(NSError*)error;
@end

@interface Cities : BaseStorage <RemoteCitiesDelegate>

@property (strong, nonatomic) NSMutableArray *citiesArray;
@property (strong, nonatomic) RemoteCities *remoteCities;

-(id) initWithDelegate:(id <CitiesDelegate>) delegate;
-(void) initCities;
-(NSMutableArray*) getCities;
-(NSInteger)getCitiesCount;
-(City*)getCityByName:(NSString*)cityName;
@end
