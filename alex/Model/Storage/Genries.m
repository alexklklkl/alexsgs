//
//  Genries.m
//  alex
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "Genries.h"

@implementation Genries

-(id) initWithDelegate:(id <GenriesDelegate>) delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.genriesArray = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)hasError:(NSError*)error {
    [self.delegate hasError:error];
}

-(void) initGenries {
    [self performSelectorInBackground:@selector(initGenriesBackground) withObject:nil];
}

-(void) initGenriesBackground {
    //TODO Раз например в месяц обновлять извне
    if ([self getGenriesCountDb] > 0) {
        NSMutableArray *genries = [self loadGenriesDb];
        [self performSelectorOnMainThread:@selector(readyGenriesForeground:) withObject:genries waitUntilDone:NO];
    } else {
        self.remoteGenries = [[RemoteGenries alloc] initWithDelegate:self];
        [self.remoteGenries initGenries];
    }
}

-(int) getGenriesCountDb {
    __block int recCount = 0;
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"select id from %@ ", GENRIESTABLENAME];
        recCount = [commonDb intForQuery:query];
    }];
    return recCount;
}

-(NSMutableArray*) loadGenriesDb {
    __block NSMutableArray *genries = [[NSMutableArray alloc] init];;
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"select %@, %@ from %@ order by %@", IDGENRE, NAME, GENRIESTABLENAME, IDGENRE];
        FMResultSet *results = [commonDb executeQuery:query];
        while ([results next]) {
            Genre *genre = [[Genre alloc] initWithId:[results stringForColumn:IDGENRE] andName:[results stringForColumn:NAME]];
            [genries addObject:genre];
        }
    }];
    return genries;
}

-(void) saveGenriesDb:(NSMutableArray *)genries {
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"delete from %@", GENRIESTABLENAME];
        [commonDb executeUpdate:query];
        for (Genre *genre in genries) {
            query = [NSString stringWithFormat:@"insert into %@ (%@, %@) values ('%@', '%@')",
                     GENRIESTABLENAME,
                     IDGENRE, NAME,
                     genre.idGenre, genre.name];
            [commonDb executeUpdate:query];
        }
    }];
}

-(void) readyGenries:(NSMutableArray *)genries {
    [self saveGenriesDb:genries];
    [self performSelectorOnMainThread:@selector(readyGenriesForeground:) withObject:genries waitUntilDone:NO];
}

-(void) readyGenriesForeground:(NSMutableArray*)genries {
    self.genriesArray = genries;
    [self.delegate readyGenries];
}

-(NSMutableArray*) getGenries {
    return self.genriesArray;
}

@end
