//
//  Films.m
//  Работа с фильмами
//  Хранение фильмов в памяти (filmsArray)
//  Чтение фильмов из локальной базы данных по условиям фильтра Filter (локально хранятся фильмы на конкредную дату для конкретного города)
//  Отправка удаленных web-запросов и получение результатов c помощью RemoteFilms (делегат для RemoteFilms)
//  Сохранение фильмов в локальной базе данных
//  Работает не в UI-потоке
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "Films.h"

@implementation Films

-(id) initWithDelegate:(id <FilmsDelegate>) delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.filmsArray = [[NSMutableArray alloc] init];
        self.remoteFilms = [[RemoteFilms alloc] initWithDelegate:self];
    }
    return self;
}

// Переадресация ошибок в делегат
-(void)hasError:(NSError*)error {
    [self.delegate hasError:error];
}

//Определим какие данные использовать - локальные или удаленные
-(void)initFilmsWithFilter:(Filter *) filter {
    BOOL needToLoadRemoteFilms = NO;
    if (!self.filter) { //Первый запуск - получить фильмы с помощью API
        needToLoadRemoteFilms = YES;
    } else if ( // Новые город или дата - получить фильмы с помощью API
               (![self.filter.city.idCity isEqualToString: filter.city.idCity]) ||
               (![[self.filter getFormatedDate] isEqualToString: [filter getFormatedDate]])
               ) {
        needToLoadRemoteFilms = YES;
    }
    self.filter = filter;
    if (needToLoadRemoteFilms) {
        //Будут новые фильмы, очистить фильтр по жанрам
        [self.filter.filterGenries removeAllObjects];
        // Получить удаленные данные с помощью API
        [self performSelectorInBackground:@selector(initRemoteFilmsBackground) withObject:nil];
    } else {
        // Используем локальные данные
        [self performSelectorInBackground:@selector(initLocalFilmsBackground) withObject:nil];
    }
}

-(void) initLocalFilmsBackground {
    // Прочитаем локальные данные с условиями фильтра self.filter
    NSMutableArray *films = [self loadFilmsDb];
    // Оповестим делегата в UI-потоке
    [self performSelectorOnMainThread:@selector(readyFilmsForeground:) withObject:films waitUntilDone:YES];
}

-(void) initRemoteFilmsBackground {
    // TODO Очищать картинки раз в месяц
    if (CLEAR_IMAGES_EVERY_RUN) {
        [self clearImages];
    }
    [self.remoteFilms initFilmsWithFilter:self.filter];
}

-(NSMutableArray*) loadFilmsDb {
    // Все фильмы целиком по текущему фильтру -
    // отбор по жанрам и сортировка по рейтингу/названию фильмов
    NSString *where = @"";
    if ([self.filter.filterGenries count] > 0) {
        where = @" where ";
        for (Genre *genre in self.filter.filterGenries) {
            NSString *item = [NSString stringWithFormat:@" %@ like '%%%@%%' or ", GENRIES, genre.name];
            where = [where stringByAppendingString:item];
        }
        where = [where substringToIndex:([where length] - 3)];
    }
    
    __block NSMutableArray *films = [[NSMutableArray alloc] init];;
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"select * from %@ %@ order by %@", FILMSTABLENAME, where, self.filter.order];
        FMResultSet *results = [commonDb executeQuery:query];
        while ([results next]) {
            Film *film = [[Film alloc] initWithId:[results stringForColumn:IDFILM] andName:[results stringForColumn:NAME]];
            [film setYear:[results stringForColumn:YEAR]];
            [film setLength:[results stringForColumn:LENGTH]];
            [film setRating:[results stringForColumn:RATING]];
            [film setCountry:[results stringForColumn:COUNTRY]];
            [film setAge:[results stringForColumn:AGE]];
            [film setGenries:[results stringForColumn:GENRIES]];
            [film setDirector:[results stringForColumn:DIRECTOR]];
            [film setRoles:[results stringForColumn:ROLES]];
            [film setDescr:[results stringForColumn:DESCR]];
            [film setTrailer:[results stringForColumn:TRAILER]];
            
            [films addObject:film];
        }
    }];
    return films;
}

-(void) saveFilmsDb:(NSMutableArray *)films {
    // Сохранение списка фильмов (только idFilm и name), детальное описание фильма еще не получено
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"delete from %@", FILMSTABLENAME];
        [commonDb executeUpdate:query];
        float r = -1;
        for (Film *film in films) {
            query = [NSString stringWithFormat:@"insert into %@ (%@, %@, %@) values ('%@', '%@', '%f')",
                     FILMSTABLENAME,
                     IDFILM, NAME, RATING,
                     film.idFilm, film.name, r];
            [commonDb executeUpdate:query];
            // Т.к. рейтинг фильмов еще не известен, сохраним порядок сортировки искусственно
            r -= 0.001;
        }
    }];
}

// Сохранение детальной информации о фильме
-(void) saveFilmDb:(Film *) film {
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"update %@ set "
                           " %@ = '%@', "
                           " %@ = '%@', "
                           " %@ = '%@', "
                           " %@ = '%@', "
                           " %@ = '%@', "
                           " %@ = '%@', "
                           " %@ = '%@', "
                           " %@ = '%@', "
                           " %@ = '%@', "
                           " %@ = '%@' "
                           " where %@ = '%@'",
                           FILMSTABLENAME,
                           YEAR, film.year,
                           LENGTH, film.length,
                           RATING, film.rating,
                           COUNTRY, film.country,
                           AGE, film.age,
                           GENRIES, film.genries,
                           DIRECTOR, film.director,
                           ROLES, film.roles,
                           DESCR, film.descr,
                           TRAILER, film.trailer,
                           IDFILM, film.idFilm];
        [commonDb executeUpdate:query];
    }];
}

// Метод-делегат для RemoteFilms - получен список фильмов с помощью API
-(void)readyFilms:(NSMutableArray *)filmsIn {
    // Сохраним массив фильмов в локальной базе
    [self saveFilmsDb:filmsIn];
    // Сразу же перечитаем данные (применится фильтр и условия сортировки)
    NSMutableArray *films = [self loadFilmsDb];
    // Оповестим делегата (FilmsTableViewController), обновится содержимое всей таблицы фильмов
    [self performSelectorOnMainThread:@selector(readyFilmsForeground:) withObject:films waitUntilDone:YES];
    // Запросим дополнительную информацию (картинки и детальное описание) для каждого фильма
    NSFileManager *fileManager = [NSFileManager defaultManager];
    for (Film *film in films) {
        NSString *imagePath = [[Helper getImagePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", film.idFilm]];
        if (![fileManager fileExistsAtPath:imagePath]) {
            // По готовности вызовется readyFilmImage
            [self.remoteFilms initFilmImage:film];
        }
        // По готовности вызовется readyFilm
        [self.remoteFilms initFilm:film];
    }
}

// Возврат в UI-поток
- (void) readyFilmsForeground:(NSMutableArray*) films {
    // Можно использовать переданные фильмы
    self.filmsArray = films;
    [self.delegate readyFilms];
}

// Получена детальная информация о фильме, сохраним и оповестим делегата (FilmsTableViewController), обновится содержимое одной ячейки таблицы с конкретным фильмом
-(void)readyFilm:(Film *) film {
    [self saveFilmDb:film];
    [self performSelectorOnMainThread:@selector(readyFilmForeground:) withObject:film waitUntilDone:NO];
}

// Возврат в UI-поток
-(void)readyFilmForeground:(Film *) film {
    [self.delegate readyFilm:film];
}

// Получена картинка (сохранена в documents/images с именем idFilm.jpg) оповестим делегата (FilmsTableViewController), обновится содержимое одной ячейки таблицы с конкретным фильмом
-(void)readyFilmImage:(Film *)film {
    [self performSelectorOnMainThread:@selector(readyFilmImageForeground:) withObject:film waitUntilDone:NO];
}

// Возврат в UI-поток
-(void)readyFilmImageForeground:(Film *)film {
    [self.delegate readyFilmImage:film];
}

#pragma mark - Other

-(NSMutableArray *) getFilms {
    return self.filmsArray;
}

-(Film *)getFilmByIndex:(NSInteger)index {
    if ((index >= 0) && (index < [self.filmsArray count])) {
        return [self.filmsArray objectAtIndex:index];
    } else {
        return nil;
    }
}

-(Film *)getFilmById:(NSString *)idFilm {
    NSInteger index = [self getFilmIndexById:idFilm];
    if ((index >= 0) && (index < [self.filmsArray count])) {
        return [self.filmsArray objectAtIndex:[self getFilmIndexById:idFilm]];
    } else {
        return nil;
    }
}

-(NSInteger)getFilmIndexById:(NSString *)idFilm {
    NSInteger size = self.filmsArray.count;
    for (NSInteger i = 0; i < size; i++) {
        if ([((Film *)[self.filmsArray objectAtIndex:i]).idFilm isEqualToString:idFilm]) {
            return i;
        }
    }
    [self.delegate hasError:[Helper getError:@"Error NO id film"]];
    return -1;
}

-(void) clearImages {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *fileArray = [fileManager contentsOfDirectoryAtPath:[Helper getImagePath] error:nil];
    for (NSString *filename in fileArray)  {
        NSString *path = [[Helper getImagePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", filename]];
        [fileManager removeItemAtPath:path error:NULL];
    }
}

@end
