//
//  Theaters.m
//  alex
//
//  Created by Alex on 09.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "Theaters.h"

@implementation Theaters
-(id) initWithDelegate:(id <TheatersDelegate>) delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.theatersArray = [[NSMutableArray alloc] init];
        self.remoteTheaters = [[RemoteTheaters alloc] initWithDelegate:self];
    }
    return self;
}

-(void)hasError:(NSError*)error {
    [self.delegate hasError:error];
}

-(void)initTheatersWithFilter:(Filter *) filter {
    [self performSelectorInBackground:@selector(initTheatersWithFilterBackground:) withObject:filter];
}

-(void)initTheatersWithFilterBackground:(Filter *) filter {
    BOOL needToLoadRemoteTheater = NO;
    if (!self.filter) {
        //Первый запуск
        //if ([self getTheatersCountDb] == 0) // город мог смениться по GPS
            needToLoadRemoteTheater = YES;
    } else if (
               // Новые город
               (![self.filter.city.idCity isEqualToString: filter.city.idCity])
               ) {
        needToLoadRemoteTheater = YES;
    }
    self.filter = filter;
    if (needToLoadRemoteTheater) {
        [self.remoteTheaters initTheatersWithFilter:filter];
    } else {
        NSMutableArray *theaters =  [self loadTheatersDb];
        [self performSelectorOnMainThread:@selector(readyTheatersForeground:) withObject:theaters waitUntilDone:NO];
    }
}

-(int) getTheatersCountDb {
    __block int recCount = 0;
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"select id from %@ ", THEATERSTABLENAME];
        recCount = [commonDb intForQuery:query];
    }];
    return recCount;
}

-(NSMutableArray*) loadTheatersDb {
    __block NSMutableArray *theaters = [[NSMutableArray alloc] init];
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"select %@, %@, %@, %@, %@, %@ from %@ order by %@", IDTHEATER, NAME, ADRESS, PHONE, LATITUDE, LONGITUDE, THEATERSTABLENAME, NAME];
        FMResultSet *results = [commonDb executeQuery:query];
        while ([results next]) {
            Theater *theater = [[Theater alloc] initWithId:[results stringForColumn:IDTHEATER] andName:[results stringForColumn:NAME]];
            theater.adress = [results stringForColumn:ADRESS];
            theater.phone = [results stringForColumn:PHONE];
            theater.latitude = [results doubleForColumn:LATITUDE];
            theater.longitude = [results doubleForColumn:LONGITUDE];
            [theaters addObject:theater];
        }
    }];
    return theaters;
}

-(void) saveTheatersDb:(NSMutableArray *)theaters {
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"delete from %@", THEATERSTABLENAME];
        [commonDb executeUpdate:query];
        for (Theater *theater in theaters) {
            query = [NSString stringWithFormat:@"insert into %@ (%@, %@, %@, %@, %@, %@) values ('%@', '%@', '%@', '%@', '%f', '%f')",
                     THEATERSTABLENAME,
                     IDTHEATER, NAME,
                     ADRESS, PHONE,
                     LATITUDE, LONGITUDE,
                     theater.idTheater, theater.name,
                     theater.adress, theater.phone,
                     theater.latitude, theater.longitude
                     ];
            [commonDb executeUpdate:query];
        }
    }];
}

-(void) readyTheaters:(NSMutableArray *)theaters {
    [self saveTheatersDb:theaters];
    //[self loadTheatersDb];
    [self performSelectorOnMainThread:@selector(readyTheatersForeground:) withObject:theaters waitUntilDone:NO];
}

-(void) readyTheatersForeground:(NSMutableArray*)theaters {
    self.theatersArray = theaters;
    [self.delegate readyTheaters];
}

-(void)readyTheaterCoordinates:(Theater *)theater {
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"update %@ set %@ = '%f', %@ = '%f' where %@ = %@",
                           THEATERSTABLENAME,
                           LATITUDE, theater.latitude,
                           LONGITUDE, theater.longitude,
                           IDTHEATER, theater.idTheater];
        [commonDb executeUpdate:query];
    }];
    Theater *storedTheater = [self getTheaterWithId:theater.idTheater];
    storedTheater.latitude = theater.latitude;
    storedTheater.longitude = theater.longitude;
}

-(NSMutableArray*) getTheaters {
    return self.theatersArray;
}

-(Theater *)getTheaterWithIndex:(NSInteger)index {
    return [self.theatersArray objectAtIndex:index];
}

-(Theater *)getTheaterWithId:(NSString *)idTheater {
    NSInteger index = [self getTheaterIndexById:idTheater];
    if ((index >= 0) && (index < [self.theatersArray count]))
        return [self.theatersArray objectAtIndex:index];
    else
        return nil;
}

-(NSInteger)getTheaterIndexById:(NSString *)idTheater {
    NSInteger size = self.theatersArray.count;
    for (NSInteger i = 0; i < size; i++) {
        if ([((Theater *)[self.theatersArray objectAtIndex:i]).idTheater isEqualToString:idTheater]) {
            return i;
        }
    }
    [Helper getError:@"Error NO id theater"];
    return -1;
}

-(CGFloat) getSpan {
    // TODO Расчитать в зависимости от расстояния между кинотеатрами
    return 0.02;
}

@end
