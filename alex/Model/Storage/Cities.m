//
//  Cities.m
//  alex
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "Cities.h"

@implementation Cities
-(id) initWithDelegate:(id <CitiesDelegate>) delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.citiesArray = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)hasError:(NSError*)error {
    [self.delegate hasError:error];
}

-(void)initCities {
    [self performSelectorInBackground:@selector(initCitiesBackground) withObject:nil];
}

-(void)initCitiesBackground {
    //TODO Раз например в месяц обновлять извне
    if ([self getCitiesCountDb] > 0) {
        NSMutableArray *cities = [self loadCitiesDb];
        [self performSelectorOnMainThread:@selector(readyCitiesForeground:) withObject:cities waitUntilDone:NO];
    } else {
        self.remoteCities = [[RemoteCities alloc] initWithDelegate:self];
        [self.remoteCities initCities];
    }
}

-(int) getCitiesCountDb {
    __block int recCount = 0;
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"select id from %@ ", CITIESTABLENAME];
        recCount = [commonDb intForQuery:query];
    }];
    return recCount;
}

-(NSMutableArray*) loadCitiesDb {
    __block NSMutableArray *cities = [[NSMutableArray alloc] init];
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"select %@, %@ from %@ order by %@", IDCITY, NAME, CITIESTABLENAME, NAME];
        FMResultSet *results = [commonDb executeQuery:query];
        while ([results next]) {
            City *city = [[City alloc] initWithId:[results stringForColumn:IDCITY] andName:[results stringForColumn:NAME]];
            [cities addObject:city];
        }
    }];
    return cities;
}

-(void) saveCitiesDb:(NSMutableArray *)cities {
    [[[Data sharedInstance] getQueue] inDatabase:^(FMDatabase *commonDb) {
        NSString *query = [NSString stringWithFormat:@"delete from %@", CITIESTABLENAME];
        [commonDb executeUpdate:query];
        for (City *city in cities) {
            query = [NSString stringWithFormat:@"insert into %@ (%@, %@) values ('%@', '%@')",
                     CITIESTABLENAME,
                     IDCITY, NAME,
                     city.idCity, city.name];
            [commonDb executeUpdate:query];
        }
    }];
}

-(void) readyCities:(NSMutableArray *)cities {
    [self saveCitiesDb:cities];
    [self performSelectorOnMainThread:@selector(readyCitiesForeground:) withObject:cities waitUntilDone:NO];
}

-(void) readyCitiesForeground:(NSMutableArray*)cities {
    self.citiesArray = cities;
    [self.delegate readyCities];
}

-(NSMutableArray*) getCities {
    return self.citiesArray;
}

-(NSInteger)getCitiesCount {
    return [self.citiesArray count];
}

-(City*)getCityByName:(NSString*)cityName {
    for (City *city in self.citiesArray) {
        if ([city.name isEqualToString:cityName]) {
            return city;
        }
    }
    return nil;
}

@end
