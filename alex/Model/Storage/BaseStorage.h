//
//  BaseStorage.h
//  alex
//
//  Created by Alex on 09.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "Helper.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "Data.h"

@interface BaseStorage : NSObject
@property (nonatomic, retain) id delegate;

-(id) init;
@end
