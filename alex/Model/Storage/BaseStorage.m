//
//  BaseStorage.m
//  alex
//
//  Created by Alex on 09.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "BaseStorage.h"

@implementation BaseStorage

-(id) init {
    self = [super init];
    if (self) {
    }
    return self;
}

-(void)hasError:(NSError*)error {
    [self.delegate hasError:error];
}

@end
