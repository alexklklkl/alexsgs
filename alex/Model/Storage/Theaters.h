//
//  Theaters.h
//  alex
//
//  Created by Alex on 09.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "BaseStorage.h"
#import "RemoteTheaters.h"
#import "Filter.h"
#import "Theater.h"

@protocol TheatersDelegate <NSObject>
-(void) readyTheaters;
-(void) hasError:(NSError*)error;
@end

@interface Theaters : BaseStorage <RemoteTheatersDelegate>

@property (strong, nonatomic) NSMutableArray *theatersArray;
@property (strong, nonatomic) RemoteTheaters *remoteTheaters;
@property (strong, nonatomic) Filter *filter;

-(void)initTheatersWithFilter:(Filter *) filter;
-(NSMutableArray *)getTheaters;
-(NSInteger)getTheaterIndexById:(NSString *)idTheater;
-(Theater *)getTheaterWithIndex:(NSInteger)index;
-(Theater *)getTheaterWithId:(NSString*)idTheater;
-(CGFloat) getSpan;
-(id) initWithDelegate:(id <TheatersDelegate>) delegate;

@end
