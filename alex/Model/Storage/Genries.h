//
//  Genries.h
//  alex
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "BaseStorage.h"
#import "RemoteGenries.h"

@protocol GenriesDelegate <NSObject>
-(void) readyGenries;
-(void) hasError:(NSError*)error;
@end

@interface Genries : BaseStorage <RemoteGenriesDelegate>

@property (strong, nonatomic) NSMutableArray *genriesArray;
@property (strong, nonatomic) RemoteGenries *remoteGenries;

-(void)initGenries;
-(id) initWithDelegate:(id <GenriesDelegate>) delegate;
-(NSMutableArray*) getGenries;

@end
