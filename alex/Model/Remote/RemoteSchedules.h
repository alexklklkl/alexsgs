//
//  RemoteSchedules.h
//  alex
//
//  Created by Alex on 09.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "Filter.h"
#import "Theaters.h"
#import "Schedule.h"

@protocol RemoteSchedulesDelegate <NSObject>
-(void)readySchedules:(NSMutableArray *)schedules;
-(void)hasError:(NSError*)error;
@end

@interface RemoteSchedules : NSObject

@property (nonatomic, assign) id <RemoteSchedulesDelegate> delegate;
@property (nonatomic, strong) NSString* apiUrlSchedules;
@property (nonatomic, strong) Theaters* theaters;

-(id) initWithDelegate:(id <RemoteSchedulesDelegate>) delegate andTeathers:(Theaters*)theaters;
-(void)initSchedulesWithFilter:(Filter*)filter;
-(void)setTheaters:(Theaters *)theaters;

@end
