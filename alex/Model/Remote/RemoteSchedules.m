//
//  RemoteSchedules.m
//  alex
//
//  Created by Alex on 09.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "RemoteSchedules.h"

@implementation RemoteSchedules
-(id) initWithDelegate:(id <RemoteSchedulesDelegate>) delegate andTeathers:(Theaters *)theaters {
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.theaters = theaters;
        self.apiUrlSchedules = APIURLSCHEDULES;
    }
    return self;
}
-(void)initSchedulesWithFilter:(Filter*)filter {
    [self performSelectorInBackground:@selector(requestSchedules:) withObject:filter];
}

-(void) requestSchedules:(Filter*)filter {
    @try {
        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat: self.apiUrlSchedules, filter.city.idCity, filter.idFilm, [filter getFormatedDate]]];
        NSError *error;
        
        NSString *dataString = [[NSString alloc]
                                        initWithContentsOfURL:URL
                                        encoding:NSUTF8StringEncoding
                                        error:&error];
        
        if (error) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error request %@\n%@", URL, [error localizedDescription]]]];
            return;
        }
        
        NSMutableArray *jsonSchedules = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        if (error) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NSJSONSerialization Schedules %@", [error localizedDescription]]]];
            return;
        }
        if ([jsonSchedules count] == 0) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NO Schedules"]]];
            return;
        }
        
        NSMutableArray *schedulesArray = [[NSMutableArray alloc] init];
        for (NSDictionary *item in jsonSchedules) {
            NSString *idTheater = [item objectForKey:IDTHEATER];
            Schedule *schedule = [[Schedule alloc] init];
            schedule.theater = [self.theaters getTheaterWithId:idTheater];
            NSMutableArray *schedulesLocal = [[NSMutableArray alloc] init];
            for (NSDictionary *itemSched in [item objectForKey:SCHEDULES]) {
                [schedulesLocal addObject:itemSched];
            }
            schedule.schedules = schedulesLocal;
            
            [schedulesArray addObject:schedule];
        }
        [self performSelectorOnMainThread:@selector(readySchedulesForeground:) withObject:schedulesArray waitUntilDone:NO];
    } @catch(NSException *e) {
        [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NO Schedules"]]];
        return;
    }
    return;
}

-(void) readySchedulesForeground:(NSMutableArray *)schedules {
    [self.delegate readySchedules:schedules];
}

@end
