//
//  RemoteTeathers.m
//  alex
//
//  Created by Alex on 09.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "RemoteTheaters.h"

@implementation RemoteTheaters
-(id) initWithDelegate:(id <RemoteTheatersDelegate>) delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.apiUrlTheaters = APIURLTHEATERS;
        self.apiUrlCoordinates = APIURLCOORDINATES;
    }
    return self;
}

-(void)initTheatersWithFilter:(Filter*)filter {
    self.filter = filter;
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat: self.apiUrlTheaters, filter.city.idCity]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultConfigObject.timeoutIntervalForRequest = 10 * 60;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
        NSURLSessionDataTask *downloadTask = [defaultSession
            dataTaskWithURL:URL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (error) {
                    NSLog(@"Error request %@\n%@", URL, [error localizedDescription]);
                    [self.delegate hasError:error];
                }
                NSString* dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                [self performSelectorInBackground:@selector(parseTheaters:) withObject:dataString];
            }];
        [downloadTask resume];        
}

-(void) parseTheaters:(NSString*)dataString {
    NSError *error;
    NSMutableArray *jsonTheaters = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    if (error) {
        [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NSJSONSerialization theaters %@", [error localizedDescription]]]];
        return;
    }
    if ([jsonTheaters count] == 0) {
        [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NO theater"]]];
    }
    NSMutableArray *theaters = [[NSMutableArray alloc] init];
    for (NSDictionary *item in jsonTheaters) {
        Theater *theater = [[Theater alloc] initWithId:[item objectForKey:IDTHEATER] andName:[item objectForKey:NAME]];
        theater.adress = [item objectForKey:ADRESS];
        theater.phone = [item objectForKey:PHONE];
        theater.latitude = [[item objectForKey:LATITUDE] floatValue];
        theater.longitude = [[item objectForKey:LONGITUDE] floatValue];
        [theaters addObject:theater];
        
    }
    [self.delegate readyTheaters:theaters];
    [self requestCoordinates:theaters];
}

-(void) requestCoordinates:(NSMutableArray*) theaters {
    for (Theater *theater in theaters) {
        NSString *nameCity = [self.filter.city.name stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        NSString *name = [theater.name stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        // TODO сделать поиск не по названию кинотеатра, а по адрусу (нужно "чистить" станции метро в адресе)
        NSString *requestUrl = [NSString stringWithFormat: self.apiUrlCoordinates, theater.idTheater, nameCity, name];
        NSURL *URLCoordinates = [NSURL URLWithString:requestUrl];
        NSError *error;
        NSString *dataString = [[NSString alloc]
                                initWithContentsOfURL:URLCoordinates
                                encoding:NSUTF8StringEncoding
                                error:&error];
        if (error) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error request %@\n%@", URLCoordinates, [error localizedDescription]]]];
            return;
        }
        NSDictionary *jsonCoordinates = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        if (error) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NSJSONSerialization coordinates %@", [error localizedDescription]]]];
            return;
        }
        if (
            ([[jsonCoordinates objectForKey:LATITUDE] floatValue] > 0) &&
            ([[jsonCoordinates objectForKey:LONGITUDE] floatValue] > 0)
            ) {
            Theater *theaterCoordinates = [[Theater alloc] initWithId:[jsonCoordinates objectForKey:IDTHEATER] andName:@""];
            theaterCoordinates.latitude = [[jsonCoordinates objectForKey:LATITUDE] floatValue];
            theaterCoordinates.longitude = [[jsonCoordinates objectForKey:LONGITUDE] floatValue];
            [self.delegate readyTheaterCoordinates:theaterCoordinates];
        }
    }
}

@end
