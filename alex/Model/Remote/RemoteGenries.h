//
//  RemoteGenries.h
//  alex
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "Genre.h"
#import "Helper.h"

@protocol RemoteGenriesDelegate <NSObject>
-(void)readyGenries:(NSMutableArray *)genries;
-(void)hasError:(NSError*)error;
@end

@interface RemoteGenries : NSObject

@property (nonatomic, assign) id <RemoteGenriesDelegate> delegate;

-(id) initWithDelegate:(id <RemoteGenriesDelegate>) delegate;
-(void)initGenries;

@end
