//
//  Получение информации о фильмах с сервера с использованием API
//  По готовности оповещается делегат (Films)
//  Работает не в UI-потоке
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "RemoteFilms.h"

@implementation RemoteFilms

-(id) initWithDelegate:(id <RemoteFilmsDelegate>) delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
        // Вызов API для получения списка фильмов
        self.apiUrlFilms = APIURLFILMS;
        // Вызов API для получения детальной информации о фильме
        self.apiUrlFilm = APIURLFILM;
        // Вызов API для получения картинки фильма
        self.apiUrlImage = APIURLIMAGE;
    }
    return self;
}

// Запрос списка фильмов (параметры - id города и дата)
// http://grad39.ru/php/kinopoisk/getFilms.php?idCity=%@&date=%@
// Ответ API - массив в формате json:
// [{"idCity":"...","name":"..."},
//  {"idCity":"...","name":"..."}]
-(void)initFilmsWithFilter:(Filter *)filter {
//    @try {
        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:self.apiUrlFilms, filter.city.idCity, [filter getFormatedDate]]];
        NSError *error;
        
        NSString *dataString = [[NSString alloc]
                                initWithContentsOfURL:URL
                                encoding:NSUTF8StringEncoding
                                error:&error];
        if (error) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error request %@\n%@", URL, [error localizedDescription]]]];
            return;
        }
        NSMutableArray *jsonFilms = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        if (error) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NSJSONSerialization Films %@", [error localizedDescription]]]];
            return;
        }
        if ([jsonFilms count] == 0) {
            [self.delegate hasError:[Helper getError:@"Error NO films"]];
            return;
        }
        // Заполняем массив и передаем делегату (Films)
        NSMutableArray *films = [[NSMutableArray alloc] init];
        for (NSDictionary *item in jsonFilms) {
            Film *film;
                film = [[Film alloc] initWithId:[item objectForKey:IDFILM] andName:[item objectForKey:NAME]];
            [films addObject:film];
        }
        [self.delegate readyFilms:films];
//    } @catch(NSException *e) {
//        [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NO films %@", e.reason]]];
//    }
    return;
}

// ------------- Картинка о фильма --------------------
#pragma FilmImage

// Загрузка картинки фильма
-(void)initFilmImage:(Film *)film {
    @try {
        // Запросим URL картинки
        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:self.apiUrlImage, film.idFilm]];
        NSError *error;
        
        NSString *dataString = [[NSString alloc]
                                initWithContentsOfURL:URL
                                encoding:NSUTF8StringEncoding
                                error:&error];
        if (error) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error request %@\n%@", URL, [error localizedDescription]]]];
            return;
        }
        
        // Получим саму картинку
        // TODO - кинопоиск имеет картинки не для всех фильмов. Проверять на сервере.
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:dataString]];
        UIImage *image = [UIImage imageWithData:imageData];

        // Изменим размер картинки (для детального просмотра) - ширина по ширине экрана, высота пропорционально - и сохраним
        NSString *imagePath = [[Helper getImagePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", film.idFilm]];
        // TODO ширину экрана передавать в запросе и делать resize на сервере 
        image = [Helper resizeImage:image andRatio:ASPECT_RATIO];
        [UIImageJPEGRepresentation(image, JPG_QUALITY) writeToFile:imagePath atomically:YES];

        // Изменим размер картинки (для ячейки таблицы) - ширина по ширине экрана, высота = ширина * CELL_RATIO, сохраним
        imagePath = [[Helper getImagePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_th.jpg", film.idFilm]];
        image = [Helper resizeImage:image andRatio:CELL_RATIO];
        [UIImageJPEGRepresentation(image, JPG_QUALITY) writeToFile:imagePath atomically:YES];
        
        // Оповестим делегата о готовности картинки
        [self.delegate readyFilmImage:film];
    } @catch(NSException *e) {
        [self.delegate hasError:[Helper getError:@"Error NO film image"]];
    }
}

// ------------- Детальная информация о фильме --------------------
#pragma Film Detail

// Запрос детальной информации о фильме (параметр - id фильма)
// http://grad39.ru/php/kinopoisk/getFilmDetail.php?idFilm=%@
// Ответ API - объект в формате json:
// {"idFilm":"946883","name":"...","year":"...","length":"...","genries":"...","country":"... ","director":"...","rating":"0.00","roles":"...","descr":"...","age":"","trailer":""}
-(void)initFilm:(Film *)film {
    @try {
        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:self.apiUrlFilm, film.idFilm]];
        NSError *error;
        
        NSString *dataString = [[NSString alloc]
                                initWithContentsOfURL:URL
                                encoding:NSUTF8StringEncoding
                                error:&error];
        if (error) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error request %@\n%@", URL, [error localizedDescription]]]];
            return;
        }
        NSDictionary *jsonFilmDetail = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        if (error) {
#ifndef DEBUG_MODE
            NSString *errorString = [NSString stringWithFormat: @"Error NSJSONSerialization FilmDetail %@ (%@) %@",
                film.name,
                film.idFilm,
                [error localizedDescription]];
            [self.delegate hasError:[Helper getError:errorString]];
#endif
            return;
        }
        if ([jsonFilmDetail count] == 0) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NO film detail for id %@", film.idFilm]]];
            return;
        }
        // idFilm и name оставим как есть - они проинициализированы в Films
        //[film setIdFilm:[jsonFilmDetail objectForKey:IDFILM]];
        //[film setName:[jsonFilmDetail objectForKey:NAME]];
        [film setYear:[jsonFilmDetail objectForKey:YEAR]];
        [film setRating:[jsonFilmDetail objectForKey:RATING]];
        [film setLength:[jsonFilmDetail objectForKey:LENGTH]];
        [film setCountry:[jsonFilmDetail objectForKey:COUNTRY]];
        [film setAge:[jsonFilmDetail objectForKey:AGE]];
        [film setGenries:[jsonFilmDetail objectForKey:GENRIES]];
        [film setDirector:[jsonFilmDetail objectForKey:DIRECTOR]];
        [film setRoles:[jsonFilmDetail objectForKey:ROLES]];
        [film setDescr:[jsonFilmDetail objectForKey:DESCR]];
        [film setTrailer:[jsonFilmDetail objectForKey:TRAILER]];
        
        [self.delegate readyFilm:film];
    } @catch(NSException *e) {
        [self.delegate hasError:[Helper getError:@"Error NO film detail"]];
    }
    return;
}

@end
