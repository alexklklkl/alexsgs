//
//  RemoteCities.h
//  alex
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "City.h"
#import "Helper.h"

@protocol RemoteCitiesDelegate <NSObject>
-(void)readyCities:(NSMutableArray *)cities;
-(void)hasError:(NSError*)error;
@end

@interface RemoteCities : NSObject
@property (nonatomic, assign) id <RemoteCitiesDelegate> delegate;
-(id) initWithDelegate:(id <RemoteCitiesDelegate>) delegate;
-(void)initCities;
@end
