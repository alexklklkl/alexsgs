//
//  RemoteTeathers.h
//  alex
//
//  Created by Alex on 09.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "Constants.h"
#import "Theater.h"
#import "Filter.h"

@protocol RemoteTheatersDelegate <NSObject>
-(void)readyTheaters:(NSMutableArray *)theaters;
-(void)readyTheaterCoordinates:(Theater *)theater;
-(void)hasError:(NSError*)error;
@end


@interface RemoteTheaters : NSObject
@property (nonatomic, assign) id <RemoteTheatersDelegate> delegate;
@property (nonatomic, strong) NSString* apiUrlTheaters;
@property (nonatomic, strong) NSString *apiUrlCoordinates;
@property (nonatomic, strong) Filter* filter;

-(id) initWithDelegate:(id <RemoteTheatersDelegate>) delegate;
-(void)initTheatersWithFilter:(Filter*)filter;
@end
