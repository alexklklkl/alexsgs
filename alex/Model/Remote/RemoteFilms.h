//
//  RemoteFilms.h
//  alex
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "Helper.h"
#import "Film.h"
#import "Filter.h"

@protocol RemoteFilmsDelegate <NSObject>
-(void)readyFilms:(NSMutableArray *)films;
-(void)readyFilmImage:(Film *)film;
-(void)readyFilm:(Film *)film;
-(void)hasError:(NSError*)error;
@end

@interface RemoteFilms : NSObject
@property (nonatomic, assign) id <RemoteFilmsDelegate> delegate;
@property (nonatomic, strong) NSString *basePath;
@property (nonatomic, assign) NSString* apiUrlFilms;
@property (nonatomic, assign) NSString* apiUrlFilm;
@property (nonatomic, assign) NSString* apiUrlImage;

-(id) initWithDelegate:(id <RemoteFilmsDelegate>) delegate;
-(void)initFilmsWithFilter:(Filter *)filter;
-(void)initFilmImage:(Film *)film;
-(void)initFilm:(Film *)film;

@end
