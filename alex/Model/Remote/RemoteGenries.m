//
//  RemoteGenries.m
//  alex
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "RemoteGenries.h"

@implementation RemoteGenries

-(id) initWithDelegate:(id <RemoteGenriesDelegate>) delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}

-(void)initGenries {
    @try {
        NSURL *URL = [NSURL URLWithString:APIURLGENRIES];
        NSError *error;
        
        NSString *dataString = [[NSString alloc]
                                initWithContentsOfURL:URL
                                encoding:NSUTF8StringEncoding
                                error:&error];
        if (error) {
            NSLog(@"Error request %@\n%@", URL, [error localizedDescription]);
            [self.delegate hasError:error];
        }
        
        NSMutableArray *jsonGenries = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        if (error) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NSJSONSerialization Genries %@", [error localizedDescription]]]];
            return;
        }
        if ([jsonGenries count] == 0) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NO genries"]]];
            return;
        }
        
        NSMutableArray *genries = [[NSMutableArray alloc] init];
        for (NSDictionary *item in jsonGenries) {
            if (![[item objectForKey:@"idGenre"] isEqualToString:@""]) {
                Genre *genre = [[Genre alloc] initWithId:[item objectForKey:IDCITY] andName:[item objectForKey:NAME]];
                [genries addObject:genre];
            }
        }
        [self.delegate readyGenries:genries];
    } @catch(NSException *e) {
        [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NO genries"]]];
        return;
    }
    return;
}
@end
