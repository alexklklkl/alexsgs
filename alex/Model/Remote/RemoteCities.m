//
//  RemoteCities.m
//  alex
//
//  Created by Alex on 28.02.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "RemoteCities.h"


@implementation RemoteCities
-(id) initWithDelegate:(id <RemoteCitiesDelegate>) delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}

-(void)initCities {
    @try {
        NSURL *URL = [NSURL URLWithString:APIURLCITIES];
        NSError *error;
        
        NSString *dataString = [[NSString alloc]
                                initWithContentsOfURL:URL
                                encoding:NSUTF8StringEncoding
                                error:&error];
        if (error) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error request %@\n%@", URL, [error localizedDescription]]]];
            return;
        }
        
        NSMutableArray *jsonCities = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        if (error) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NSJSONSerialization Cities %@", [error localizedDescription]]]];
            return;
        }
        if ([jsonCities count] == 0) {
            [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NO cities"]]];
            return;
        }
        
        NSMutableArray *cities = [[NSMutableArray alloc] init];
        for (NSDictionary *item in jsonCities) {
            City *city = [[City alloc] initWithId:[item objectForKey:IDCITY] andName:[item objectForKey:NAME]];
            [cities addObject:city];
            
        }
        [self.delegate readyCities:cities];
    } @catch(NSException *e) {
        [self.delegate hasError:[Helper getError:[NSString stringWithFormat:@"Error NO cities"]]];
        return;
    }
    return;
}
@end
