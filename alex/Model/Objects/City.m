//
//  City.m
//  alex
//
//  Created by Alex on 18.11.16.
//  Copyright © 2016 kode. All rights reserved.
//

#import "City.h"

@implementation City

-(id) initWithId:(NSString *)idCity andName:(NSString *)name
{
    self = [super init];
    if (self) {
        self.idCity = idCity;
        self.name = name;
    }
    return self;
}

@end
