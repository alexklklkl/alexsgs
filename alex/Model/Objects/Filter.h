//
//  Settings.h
//  alex
//
//  Created by Alex on 04.01.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Genre.h"
#import "City.h"

@interface Filter : NSObject
@property (nonatomic, strong) City *city;
@property (nonatomic, strong) NSMutableArray *filterGenries;
@property (nonatomic, strong) NSString *order;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *idTheatre;
@property (nonatomic, strong) NSString *idFilm;

-(id)init;
-(void)setDefaults;
-(NSString*)getFormatedDate;
-(NSString*)getDateNameForFilterDate;
-(NSString*)getDateNameForDate:(NSDate*)date;
-(NSString*)getCityName;
-(Filter*)getFilterCopy;
-(NSInteger) indexGenreInFilter:(Genre*)genre;
-(void)removeGenreFromFilter:(Genre*)genre;
-(void)addGenreToFilter:(Genre*)genre;
+(City*)getStoredCity;
-(void)storeCity;
@end
