//
//  Film.m
//  alex
//
//  Created by Alex on 18.11.16.
//  Copyright © 2016 kode. All rights reserved.
//

#import "Film.h"

@implementation Film

-(id) initWithId:(NSString *)idFilm andName:(NSString *)name
{
    self = [super init];
    if (self) {
        self.idFilm = idFilm;
        self.name = name;
        
        self.year = @"";
        self.length = @"";
        self.rating = @"";
        self.country = @"";
        self.age = @"";
        self.genries = @"";
        self.director = @"";
        self.roles = @"";
        self.descr = @"";
        self.trailer = @"";
    }
    return self;
}


@end
