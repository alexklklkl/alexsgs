//
//  City.h
//  alex
//
//  Created by Alex on 18.11.16.
//  Copyright © 2016 kode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject
@property (strong, nonatomic) NSString *idCity;
@property (strong, nonatomic) NSString *name;

-(id)initWithId:(NSString *)idCity andName:(NSString *)name;

@end
