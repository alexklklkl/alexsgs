//
//  Theatre.m
//  alex
//
//  Created by Alex on 18.11.16.
//  Copyright © 2016 kode. All rights reserved.
//

#import "Theater.h"

@implementation Theater

-(id) initWithId:(NSString *)idTheater andName:(NSString *)name
{
    self = [super init];
    if (self) {
        self.idTheater = idTheater;
        self.name = name;
        self.adress = @"";
        self.phone = @"";
        self.latitude = 0;
        self.longitude = 0;
    }
    return self;
}

@end
