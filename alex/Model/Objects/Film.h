//
//  Film.h
//  alex
//
//  Created by Alex on 18.11.16.
//  Copyright © 2016 kode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Film : NSObject
@property (strong, nonatomic) NSString *idFilm;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *year;
@property (strong, nonatomic) NSString *length;
@property (strong, nonatomic) NSString *rating;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *age;
@property (strong, nonatomic) NSString *genries;
@property (strong, nonatomic) NSString *director;
@property (strong, nonatomic) NSString *roles;
@property (strong, nonatomic) NSString *descr;
@property (strong, nonatomic) NSString *trailer;


-(id)initWithId:(NSString *)idFilm andName:(NSString *)name;
@end
