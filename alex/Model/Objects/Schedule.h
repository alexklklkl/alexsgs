//
//  Schedule.h
//  alex
//
//  Created by Alex on 09.03.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Theater.h"

@interface Schedule : NSObject
@property (nonatomic, strong) Theater *theater;
@property (nonatomic, strong) NSMutableArray *schedules;
@end
