//
//  Genre.m
//  alex
//
//  Created by Alex on 06.12.16.
//  Copyright © 2016 kode. All rights reserved.
//

#import "Genre.h"

@implementation Genre
-(id) initWithId:(NSString *)idGenre andName:(NSString *)name {
    self = [super init];
    if (self) {
        self.idGenre = idGenre;
        self.name = name;
    }
    return self;
}
@end
