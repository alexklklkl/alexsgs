//
//  Genre.h
//  alex
//
//  Created by Alex on 06.12.16.
//  Copyright © 2016 kode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Genre : NSObject
@property (strong, nonatomic) NSString *idGenre;
@property (strong, nonatomic) NSString *name;

-(id)initWithId:(NSString *)idGenre andName:(NSString *)name;
@end
