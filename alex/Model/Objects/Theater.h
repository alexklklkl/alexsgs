//
//  Theatre.h
//  alex
//
//  Created by Alex on 18.11.16.
//  Copyright © 2016 kode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Theater : NSObject
@property (strong, nonatomic) NSString *idTheater;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *adress;
@property (strong, nonatomic) NSString *phone;
@property (nonatomic) CGFloat latitude;
@property (nonatomic) CGFloat longitude;


-(id)initWithId:(NSString *)idTheater andName:(NSString *)name;
@end
