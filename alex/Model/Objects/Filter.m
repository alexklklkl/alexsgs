//
//  Filter.m
//  Класс описывает фильтр, по которому происходит загрузка данных, отбор и сортироввка
//
//  Created by Alex on 04.01.17.
//  Copyright © 2017 kode. All rights reserved.
//

#import "Constants.h"
#import "Filter.h"

@implementation Filter

-(id)init {
    self = [super init];
    if (self) {
        self.city = [[City alloc] initWithId:@"" andName:@""];
        self.filterGenries = [[NSMutableArray alloc] init];
        self.order = ORDERRATING;
        self.date = [NSDate date];
        self.idTheatre = @"";
        self.idFilm = @"";
    }
    return self;
}

// Инициализация, но с городом по умолчани.
-(void)setDefaults {
    self.city = [[City alloc] initWithId:DEFAULT_CITY_ID andName:DEFAULT_CITY_NAME];
    self.filterGenries = [[NSMutableArray alloc] init];
    self.order = ORDERRATING;
    self.date = [NSDate date];
    self.idTheatre = @"";
    self.idFilm = @"";
}

//Сохранение города в NSUserDefaults
-(void)storeCity {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.city.idCity forKey:CITY_ID_KEY];
    [defaults setObject:self.city.name forKey:CITY_NAME_KEY];
    [defaults synchronize];
}

// Получение города из NSUserDefaults
+(City*)getStoredCity {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *idCity = [defaults objectForKey:CITY_ID_KEY];
    NSString *nameCity = [defaults objectForKey:CITY_NAME_KEY];
    if (idCity) {
        City *city = [[City alloc] initWithId:idCity andName:nameCity];
        return city;
    }
    return nil;
}

// Форматированная дата для DB
-(NSString *)getFormatedDate {
    NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return [formatter stringFromDate:self.date];
}

// "ЧПУ" для даты - Сегодня, Завтра, ...
-(NSString*) getDateNameForDate:(NSDate*) dt {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc]
                        initWithLocaleIdentifier:@"ru"];
    [formatter setLocale:locale];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSTimeInterval secondsBetween = [dt timeIntervalSinceDate:[NSDate date]];
    int numberOfDays = round(secondsBetween / 86400.0);
    NSString *dateName = @"";
    if (numberOfDays == 0) {
        dateName = NSLocalizedString(@"Сегодня", nil);
    } else if (numberOfDays == 1) {
        dateName = NSLocalizedString(@"Завтра", nil);
    } else {
        [formatter setDateFormat:@"EEEE"];
        dateName = [[formatter stringFromDate: dt] capitalizedString];
    }
    return dateName;
}

// "ЧПУ" для даты фильтра
-(NSString*) getDateNameForFilterDate {
    return [self getDateNameForDate:self.date];
}

//Копия фильтра (для выбора фильтров по жанру, города и т.д.). Используем копию, т.к. пользователь может отменить свой выбор
-(Filter*)getFilterCopy {
    Filter *filter = [[Filter alloc] init];
    
    filter.city = [[City alloc] initWithId:self.city.idCity andName:self.city.name];
    for (Genre *genre in self.filterGenries) {
        [filter.filterGenries addObject:genre];
    }
    filter.order = self.order;
    filter.date = self.date;
    filter.idTheatre = self.idTheatre;
    return filter;
}

#pragma mark Other

-(NSString*)getCityName {
    return self.city.name;
}

-(NSInteger) indexGenreInFilter:(Genre*)genre {
    for (NSInteger i = 0; i < [self.filterGenries count]; i++) {
        if ([((Genre *)[self.filterGenries objectAtIndex:i]).name isEqualToString:genre.name])
            return i;
    }
    return -1;
}

-(void)removeGenreFromFilter:(Genre*)genre {
    NSInteger index = [self indexGenreInFilter:genre];
    if (index != -1)
        [self.filterGenries removeObjectAtIndex:index];
}

-(void)addGenreToFilter:(Genre*)genre {
    NSInteger index = [self indexGenreInFilter:genre];
    if (index == -1)
        [self.filterGenries addObject:genre];
}

@end
